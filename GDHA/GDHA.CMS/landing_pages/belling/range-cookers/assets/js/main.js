// Strict Mode is a new feature in ECMAScript 5 that allows you to place a program, or a function, in a "strict" operating context.
// This strict context prevents certain actions from being taken and throws more exceptions.
// And:

// Strict mode helps out in a couple ways:

// It catches some common coding bloopers, throwing exceptions.
// It prevents, or throws errors, when relatively "unsafe" actions are taken (such as gaining access to the global object).
// It disables features that are confusing or poorly thought out.

// When the below is set to true, the comment below enables use strict globally

/*jshint strict: false */

(function() {
    'use strict';

    // Global variables object
    var firstLoad = true;

    // Open video Overlay, and add markup (if already removed)
    function OpenVideoOverlay() {
        $('.btn-video').each(function() {



            var videoOverlayHtml = $(this).next().children().clone();
            $(this).on('click', function(e) {
                console.log('videoOverlayHtml', videoOverlayHtml);
                e.preventDefault();
                if (!firstLoad) {
                    $(this).next().html(videoOverlayHtml);
                    console.log("In");
                }
                $(this).next().addClass('active');
                $('.carousel-control').hide();
                firstLoad = false;
            });

        });
    }

    // Remove Overlay, and markup (to stop playing of video)
    function removeVideoOverlay() {
        $('.video-overlay-container').each(function() {
            $(this).on('click', function() {
                $('.carousel-control').show();
                $(this).removeClass('active');
                $(this).html('');
                //$(this).children().remove();
            });
        });
    }

    OpenVideoOverlay();
    removeVideoOverlay();

    var rangeDetail = $('.range-detail');
    if ($(window).width() < 768) {
        setupMobileCarousel();
    }
    stylePicker();

    $('.slick-container').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        speed: 600,
        lazyLoad: 'progressive',
        responsive: [{
            breakpoint: 989,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    $('.carousel').carousel({
    pause: true,
    interval: false
});

    // Resize delay
    var rtime = new Date(946728000);
    var timeout = false;
    var delta = 200;
    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    function resizeend() {
        if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
        } else {
            timeout = false;
            if ($(window).width() < 768) {
                setupMobileCarousel();
            } else {
                $('.mobile-slider').removeClass('carousel-inner');

                rangeDetail.each(function() {
                    $(this).css('height', '');
                });
            }
        }
    }

    function setupMobileCarousel() {
        if (rangeDetail.length > 0) {
            var tallestElementHeight = 0;
            rangeDetail.each(function() {
                $(this).css('height', '');
                if ($(this).outerHeight() > tallestElementHeight) {
                    tallestElementHeight = $(this).outerHeight();
                }
            });
            rangeDetail.each(function() {
                $(this).css('height', tallestElementHeight);
            });
        }
        $('.mobile-slider').addClass('carousel-inner');
    }

    function stylePicker() {

        $('.style-picker li').click(function(e) {

            e.preventDefault();

            var style = $(this).data('picker');

            $('.style-picker li.active span').html('View');
            $('.style-picker li.active').removeClass('active');

            $(this).addClass('active');
            $('.style-picker li.active span').html('Viewing');

            $('.style-block.active').removeClass('active');
            $('.style-block[data-style=\'' + style + '\']').addClass('active');
        });

        $('.style-picker-select').on('change', function() {
            var selectedIndex = $('.style-picker-select option:selected').index();

            if (selectedIndex !== null && selectedIndex !== undefined) {
                /* increment index in order to match with select element data attributes */
                selectedIndex++;

                var activeStyleBlock = $('.style-block.active');
                var targetStyleBlock = $('.style-block[data-style=' + selectedIndex + ']');

                if (activeStyleBlock.length > 0) {
                    activeStyleBlock.removeClass('active');

                    if (!targetStyleBlock.hasClass('active')) {
                        targetStyleBlock.addClass('active');
                    }
                }
            }
        });
    }

}());

(function() {
    // this anonymous function is sloppy...
}());
