﻿$(document).on('click', '.compare-product.empty', function(e) {
   $(".product-comparison-popup").toggleClass('popup-active');
});
$(document).ready(function () {

    $(".product-comparison-popup .expander, .product-comparison-popup .module-compare").click(function () {
        $(".product-comparison-popup").toggleClass('popup-active');
    })
   

    /*
    $('.compare-bar-expander').click(function (e) {
        var bar = $('.compare-bar');
        
        if (bar.hasClass('compare-expanded'))
        {
            bar.removeClass('compare-expanded');
            bar.animate({ height: '40px' }, 600);
        }
        else {
            var boxHeight = ($(window).height() - 170) ;

            bar.addClass("compare-expanded");
            bar.animate({ height: boxHeight + 'px' }, 600);
            $('.comparison-box').height((boxHeight - 40) + 'px');
        }
    })
    */
    $(document).on("click", '.compare-btn',function (event) {

        var site = '';

        if (window.location.hostname.indexOf('stovesuk.de') != -1)
        {
            site = 'german';
        }

        console.log('>> add to compare', $(this).hasClass('added'));

        var id = $(this).data('id');

        //&& angular.element('#comparisonController').scope().ids 
        var t = angular.element('#comparisonController').scope().ids;
        var double = $.inArray(id, t);
        if ((double === -1 || !$(this).hasClass('added')) && angular.element('#comparisonController').scope().ids.length < 4) {
            angular.element('#comparisonController').scope().add(id);
            //angular.element('#comparisonController').scope()

            if (site == 'german') {
                $(this).text("Hinzugefügt, um zu vergleichen");
            }
            else {
                $(this).text("added to compare");
            }
            
            $(this).toggleClass('added');
        }
        else if(double != -1) {
            angular.element('#comparisonController').scope().remove(id);

            if (site == 'german') {
                $(this).text("Vergleichen");  
            }
            else {
                $(this).text("Compare");
            }

        } else {

            var maxItemsError = 'you can only compare 4 items';

            if(site == 'german'){
                maxItemsError = 'sie können nur vergleichen 4 das ding';
            }

            angular.element('#comparisonController').scope().errText = maxItemsError;
        }
        angular.element('#comparisonController').scope().$apply();
        event.preventDefault();
    });
});