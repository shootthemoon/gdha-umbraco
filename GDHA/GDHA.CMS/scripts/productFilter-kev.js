﻿$(function () {
    $('.product-filter-checkbox').click(function () {
        var formdata = $("#productFilterForm").serialize();
        var rootId = $('#rootId').val();
        postFormFilter(rootId, formdata);
    });
});

function postFormFilter(nodeId, formData) {
    $.ajax({
        url: '/umbraco/uniform/ProductFilterApi/GetFilteredProductIds/' + nodeId,
        type: 'POST',
        data: formData,
        datatype: 'application/x-www-form-urlencoded',
        success: function (products) {
            var results = $('#productFilterResults');
            results.empty();
            
            if (products.length == 0)
            {
                results.append("<h3>No Results found</h3>");
            }

            $.each(products, function (index, product) {
                displayProduct(results, product);
            });
        },
        error: function (xhr) {
            var results = $('#productFilterResults');
            results.empty();
            results.append("<h3>Error finding results</h3>");
            results.append("<span>" + xhr + "</span>");
        }
    })
}

function displayProduct(results, product) {
    // here you prodibly want to plum in your favorite templating language
    // like backbone.js or moustach or something ?  

    var pDiv = $('<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center product-container"></div>')
    pDiv.append('<img src="' + product.ImgUrl + '">');
    pDiv.append('<div class="h5">' + product.Name + '</div>');
    pDiv.append(product.Description);
    pDiv.append('<div class="h6">Colours</div>');
    var colDiv = $('<div class="product-colours"></div>');

    $.each(product.Colours, function (n, colour) {
        colDiv.append('<span class="product-color">' + colour + '</span> ');
    });

    pDiv.append(colDiv);
    pDiv.append('<a class="btn btn-default" href="' + product.Url + '">View Details &raquo;</a>');
    results.append(pDiv);


}