﻿
angular.module('gdha', ['ngCookies']).controller('comparisonController', function ($scope, $http, $cookies) {
    $scope.name = "Product Comparisons";
    $scope.errText = "";
    $scope.ids = [];

    var cookie = $cookies.get('comparison');
    if (cookie != undefined)
    {
        $scope.ids = JSON.parse('[' + cookie.replace(/'/g, '"') + ']');
    }

    $scope.getComparisons = function () {

        console.log($scope.culture);

        var postData = {
            products: $scope.ids,
            culture: $scope.culture
        }

        $http({
            url: '/umbraco/api/comparison/products',
            method: "POST",
            data: postData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(function (result) {
            console.log(result);
            $scope.comparisons = result.data.comparisons;
            $scope.products = result.data.products;
            $scope.count = result.data.count;
        });
    }

    $scope.remove = function(id)
    {
        console.log('>> compare controller - remove');
        $scope.errText = "";

        // take the product id out of the list      
        var index = $scope.ids.indexOf(id);
        $scope.ids.splice(index, 1);
        console.log('removed', $scope.ids);

        // go get the product comparison for the remaining ones...
        $scope.getComparisons();

        $cookies.put('comparison', $scope.ids, { path: '/' });
        if ($scope.ids.length === 0) {
            window.location.reload();
        }
    }
    $scope.hasId = function (id) {
        if ($scope.ids.indexOf(id) > -1)
            return true;
        else return false;
    }
    $scope.add = function(id)
    {
        console.log('>> compare controller - add');

        $scope.errText = "";
        if ($scope.ids.length < 4) {
            var i = $scope.ids.indexOf(id);
            if (i == -1) {
                $scope.ids.push(id);
            }
        }
        else {
            $scope.errText = "you can only compare 4 items";
        }
        $cookies.put('comparison', $scope.ids, { path: '/' });

        $scope.getComparisons();
    }

    $scope.update = function()
    {
        var cookie = $cookies.get('comparison');
        if (cookie != undefined) {
            $scope.ids = JSON.parse('[' + cookie.replace(/'/g, '"') + ']');
        }

        $scope.getComparisons();
    }

    $scope.$watch('culture', function () {
        console.log('culture change:', $scope.culture);
        $scope.getComparisons();
    });

    $scope.addToComparison = function (args) {
        console.log('adding', args);
        $scope.add(args);
    };


    $scope.addToBasket = function($event, id)
    {
        //var cart = new Cart('samples');
        //cart.Add(id); (can't do this - jquery/angular cookies get mixed up)

        // Not ideal this is the cart.Add function from ShoppingBasket.js

        var cookie = $cookies.get('basket.samples');
        var bIds = [];
        if (cookie != undefined) {
            bIds = JSON.parse('[' + cookie.replace(/'/g, '"') + ']');
        }

        if (bIds.indexOf(id) == -1) {
            bIds.push(id);
        }

        var cookiePath = '/' + window.location.pathname.split('/', 2)[1];
        $cookies.put('basket.samples', bIds, { path: cookiePath });

        $event.target.textContent = "Added to basket";
    }
});

function addToCookie(data) {
    var scope = angular.element(document.getElementById('comparisonController')).scope();
    scope.add(data);
};