$(function () {
    $('.product-filter-checkbox').click(function () {
        var formdata = $("#productFilterForm").serialize();
        var rootId = $('#rootId').val();
        postFormFilter(rootId, formdata);
    });
});

function postFormFilter(nodeId, formData) {
    $.ajax({
        url: '/umbraco/uniform/ProductFilterApi/GetFilteredProductIds/' + nodeId,
        type: 'POST',
        data: formData,
        datatype: 'application/x-www-form-urlencoded',
        success: function (products) {
            var results = $('#productFilterResults');
            results.empty();
            
            if (products.length == 0)
            {
                results.append("<h3>No Results found</h3>");
            }

            $.each(products, function (index, product) {
                displayProduct(results, product);
            });
        },
        error: function (xhr) {
            var results = $('#productFilterResults');
            results.empty();
            results.append("<h3>Error finding results</h3>");
            results.append("<span>" + xhr + "</span>");
        }
    })
}

function displayProduct(results, product) {
    // here you prodibly want to plum in your favorite templating language
    // like backbone.js or moustach or something ?  

    var pDiv = $('<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center product-container"></div>')
    pDiv.append('<a class="" href="' + product.Url + '"><img src="' + product.ImgUrl + '">');
    pDiv.append('<div class="h5">' + product.Name + '</div></a>');
    pDiv.append(product.Description);
	if(!!product.EnergyRating) {
		if(window.location.href.indexOf('stovesuk.de') > -1) {
			pDiv.append('<div class="h6">Energieklass: ' + product.EnergyRating + '</div>');
		} else {
			pDiv.append('<div class="h6">Energy Rating: ' + product.EnergyRating + '</div>');
		}
	}

	if(window.location.href.indexOf('stovesuk.de') > -1) {
		pDiv.append('<div class="h6">Farbe</div>');
	} else {
		pDiv.append('<div class="h6">Colours</div>');
	}
	
    var colDiv = $('<div class="product-colours"></div>');

    $.each(product.Colours, function (n, colour) {
        if (colour == "#FFFFFF") {
            colDiv.append('<div class="product-color white" style="background:' + colour + '"> </div> ');
        } else{
            colDiv.append('<div class="product-color" style="background:' + colour + '"> </div> ');
        };
        
    });
    pDiv.append(colDiv);

	if(window.location.href.indexOf('stovesuk.de') > -1) {
		pDiv.append('<a class="btn btn-green" href="/haendler/?sku=' + product.Sku + '">Kaufe jetzt</a>');
		pDiv.append('<a class="btn btn-black compare-btn space-compare" data-id="' + product.Id + '">Vergleichen</a>');
	} else {
		pDiv.append('<a class="btn btn-green" href="/where-to-buy/?sku=' + product.Sku + '">Buy Now</a>');
		pDiv.append('<a class="btn btn-black compare-btn space-compare" data-id="' + product.Id + '">Compare</a>');
	}
    
	if(typeof RevooMark !== 'undefined') {
		pDiv.append('<div class="review"><a class="reevoomark" href="http://mark.reevoo.com/partner/'+product.Reevoo+'/'+product.Sku+'">Review</a></div><br>');
	}
    // pDiv.append('<div class="h6">compare</div>');
    results.append(pDiv);

	if(typeof RevooMark !== 'undefined') {
		ReevooMark.init_badges();
	}
}
