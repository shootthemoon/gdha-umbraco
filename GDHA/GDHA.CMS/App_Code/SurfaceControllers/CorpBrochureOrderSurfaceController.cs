﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Uniform.Web.Models;

namespace Uniform.Web.SurfaceControllers
{
    public sealed class CorpBrochureOrderSurfaceController : SurfaceController
    {
        [HttpPost]
        public ActionResult SendBrochureForm(CorpBrochureOrderModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            // process the form
            var isHtml = false;
            var emailFrom = this.CurrentPage.GetPropertyValue<string>("emailFrom") ?? "no-reply@localhost";
            var emailTo = this.CurrentPage.GetPropertyValue<string>("emailTo");
            var emailSubject = this.CurrentPage.GetPropertyValue<string>("emailSubject");
            var emailBody = this.CurrentPage.GetPropertyValue<string>("emailBody");

            if (!string.IsNullOrEmpty(emailTo) && !string.IsNullOrEmpty(emailSubject))
            {
                // process the email body text
                emailBody = this.ReplaceShortcodes(emailBody, Request.Form);

                // send the email
                umbraco.library.SendMail(emailFrom, emailTo, emailSubject, emailBody, isHtml);

                // store data in CSV
                this.LogContactForm(this.CurrentPage.Id, model, Request.Form);
            }

            return RedirectToUmbracoPage(this.CurrentPage.Descendants("Brochureorderthankyou").FirstOrDefault() ?? this.CurrentPage);
        }

        private string ReplaceShortcodes(string content, NameValueCollection replacements)
        {
            foreach (string key in replacements.AllKeys)
            {
                var shortcode = string.Concat('{', key, '}');
                content = content.Replace(shortcode, replacements[key].Replace(",false",""));
            }

            return content;
        }

        private void LogContactForm(int nodeId, CorpBrochureOrderModel model, NameValueCollection form)
        {
            try
            {
                var path = IOHelper.MapPath("~/App_Data/TEMP/FormData/BrochureOrders/Corporate");

                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                string filePath = string.Format("{0}\\{1}-corp-brochure-form.csv", path, DateTime.Today.ToString("yyyy-MM-dd"));

                bool addHeader = !System.IO.File.Exists(filePath);

                using (var sw = System.IO.File.AppendText(filePath))
                {
                    if (addHeader)
                    {
                        var header = new List<string>();

                        header.Add("Title");
                        header.Add("FirstName");
                        header.Add("LastName");
                        header.Add("Email");
                        header.Add("Telephone");
                        header.Add("Company");
                        header.Add("PropertyNumber");
                        header.Add("Street");
                        header.Add("Town");
                        header.Add("County");
                        header.Add("Postcode");
                        header.Add("DeliveryToGDHA");
                        header.Add("GivenConsentToBeContacted");
                        header.Add("HeardFrom");
                        header.Add("HeardFromOther");
                        header.Add("Brochure");
                        header.Add("BrochureQty");

                        sw.WriteLine("\"" + string.Join("\",\"", header.ToArray()) + "\"");
                    }

                    foreach (var brochure in model.Content.Descendants("Brochure"))
                    {
                        var singleId = string.Format("X_{0}_Singles", brochure.Name.Replace(" ", "").Trim());
                        var boxesId = string.Format("X_{0}_boxes", brochure.Name.Replace(" ", "").Trim());

                        string singles = form[singleId];
                        string boxes = form[boxesId];

                        if (!string.IsNullOrEmpty(singles) || !string.IsNullOrEmpty(boxes))
                        {
                            var values = new List<string>();

                            values.Add(model.Title);
                            values.Add(model.FirstName);
                            values.Add(model.Surname);
                            values.Add(model.Email);
                            values.Add(model.Telephone);
                            values.Add(model.Company);
                            values.Add(model.PropertyNumber);
                            values.Add(model.Street);
                            values.Add(model.Town);
                            values.Add(model.County);
                            values.Add(model.Postcode);
                            values.Add(model.DeliverToGDHA.ToString());
                            values.Add(model.ConsentToContact.ToString());
                            values.Add(model.Heardfrom);
                            values.Add(model.HeardFromBox);
                            values.Add(brochure.Name);

                            int boxCount = 0;

                            if (brochure.GetPropertyValue<bool>("allowOrderOfBoxes") && !string.IsNullOrEmpty(boxes) && int.TryParse(boxes, out boxCount))
                            {
                                int brochureCount = brochure.GetPropertyValue<int>("numberOfBrochuresInOneBox");

                                values.Add((brochureCount * boxCount).ToString());
                            }
                            else
                            {
                                values.Add(singles);
                            }

                            sw.WriteLine("\"" + string.Join("\",\"", values.ToArray()) + "\"");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error<CorpBrochureOrderSurfaceController>("There was an error logging the contact form data.", ex);
            }
        }
    }
}
