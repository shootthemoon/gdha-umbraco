﻿using System;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;
using Umbraco.Web.Mvc;
using Uniform.Web.Models;

namespace Uniform.Web.SurfaceControllers
{
	public class NewsletterFormSurfaceController : SurfaceController
	{
		[HttpPost]
		public ActionResult NewsletterSignup(NewsletterFormModel model)
		{
			if (!ModelState.IsValid)
				return CurrentUmbracoPage();

			// process the form
			if (!string.IsNullOrEmpty(model.Email))
			{
				// store data in CSV
				this.LogNewsletterForm(this.CurrentPage.Id, model.Email);
			}

			// redirect to the "thank you" page.
			return RedirectToUmbracoPage(this.CurrentPage.Children.FirstOrDefault());
		}

		private void LogNewsletterForm(int nodeId, string email)
		{
			try
			{
				var path = IOHelper.MapPath("~/App_Data/TEMP/FormData");

				if (!System.IO.Directory.Exists(path))
					System.IO.Directory.CreateDirectory(path);

				using (var sw = System.IO.File.AppendText(string.Format("{0}/newsletter-form-{1}.csv", path, nodeId)))
				{
					sw.WriteLine(email);
				}

			}
			catch (Exception ex)
			{
                LogHelper.Error<NewsletterFormSurfaceController>("There was an error logging the newsletter form data.", ex);
			}
		}
	}
}