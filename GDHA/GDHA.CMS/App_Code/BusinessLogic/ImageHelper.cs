﻿using System;
using Umbraco.Core.IO;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Diagnostics;


namespace Uniform.GDHA.BusinessLogic.Razor
{
    public static class ImageHelper
    {

        public static string GetCropImage(object mediaId, string text, int width, int height, string cropName)
        {
            try
            {
                if (mediaId != null && Int32.Parse(mediaId.ToString()) > 0)
                {
                    //var media = uQuery.GetMedia(Int32.Parse(mediaId.ToString()));
                    //string fileLocation = media.GetImageUrl();
                    //string cropLocation = media.GetImageCropperUrl("crops", cropName);

                    var umbracoHelper = new Umbraco.Web.UmbracoHelper(UmbracoContext.Current);
                    var m = umbracoHelper.Media((int)mediaId);
                    string fileLoc = m.GetResponsiveImageUrl(270, 161);
                    string cropLoc = m.GetCropUrl("umbracoFile", cropName);

                    if (m != null && System.IO.File.Exists(IOHelper.MapPath(fileLoc)))
                    {
                        return fileLoc;
                        //return string.Format("{0}", (!string.IsNullOrEmpty(cropLoc)) ? cropLoc : fileLoc);                       
                    }
                }
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.ToString());
            }

            return string.Format("http://placehold.it/{0}x{1}.png/c3c3c3/ffffff/&text={2}", width, height, System.Web.HttpUtility.UrlEncode(text));
        }

        public static string GetImage(object mediaId, string text, int width, int height)
        {
            if (mediaId != null && !string.IsNullOrEmpty((string)mediaId))
            {
                IMedia media = ApplicationContext.Current.Services.MediaService.GetById(int.Parse(mediaId.ToString()));
                
                string fileLocation = media.GetValue("umbracoFile").ToString();

                if (media != null && System.IO.File.Exists(IOHelper.MapPath(fileLocation)))
                {
                    return string.Format("/ImageGen.ashx?image={0}&width={1}&height={2}", media.GetValue("umbracoFile").ToString(), width, height);
                }
            }

            return string.Format("http://placehold.it/{0}x{1}.png/c3c3c3/ffffff/&text={2}", width, height, System.Web.HttpUtility.UrlEncode(text));
        }
    }
}
