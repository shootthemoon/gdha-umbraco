﻿using System.ComponentModel.DataAnnotations;

namespace Uniform.Web.Models
{
	public class ContactFormModel
	{
		[Required]        
		public string Title { get; set; }

		[Required]
		public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

		[Required]
		[Display(Name = "Phone number")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "Please enter a valid Phone number")]
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
		public string Telephone { get; set; }

		[Required]
		[Display(Name = "Your Question", Description = "If your question is about a product, please include the Model Number and Serial Number.")]
		public string Question { get; set; }

        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }

        [Display(Name = "Model Number")]
        public string ModelNumber { get; set; }
	}
}