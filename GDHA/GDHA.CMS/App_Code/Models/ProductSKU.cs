﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniform.Web.Models
{

    public class ProductSKU
    {
        public string SKU { get; set; }
        public string Url { get; set; }
    }

}