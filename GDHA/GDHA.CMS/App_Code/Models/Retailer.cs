﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniform.Web.Models
{

    public class Category
    {
        public string Name { get; set; }  
    }

    public class Retailer
    {
        public int id { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public string address { get; set; }
        public string disabled { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string web { get; set; }
        public string local { get; set; }
        public List<Category> categories { get; set; }
        public string category { get; set; }
        public bool isElite { get; set; }
    }
   
}
