﻿using System.Collections.Generic;
using Umbraco.Core.Models;

namespace Uniform.Web.Models
{
    public class SearchModel
    {
        public string Query { get; set; }
        public IEnumerable<IPublishedContent> SearchResults { get; set; }
    }
}
