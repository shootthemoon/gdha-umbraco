﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Uniform.Web.Models;

namespace Uniform.Web.App_Code
{
    public class ProductsController : UmbracoApiController
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public HttpResponseMessage GetAllProducts(int rootID)
        {
            try
            {
                IEnumerable<IPublishedContent> products = Umbraco.TypedContentAtRoot()
                    .FirstOrDefault(x => x.Id == rootID)
                    .Descendants("ProductSku");

                List<ProductSKU> skus = new List<ProductSKU>();

                foreach(IPublishedContent product in products)
                {
                    skus.Add(new ProductSKU()
                    {
                        SKU = product.Name,
                        Url = product.Url
                    });
                }

                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(skus))
                };

                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;

            }
            catch (Exception ex)
            {
                LogHelper.Error<Retailer>("Problem with fetching all products", ex);
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
        }
    }
}
