﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Uniform.Web.Models;

namespace Uniform.Web.App_Code
{
    public class RetailersController : UmbracoApiController
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public HttpResponseMessage GetAllRetailers(int id, string brand,string sku,string range = "",bool includeElite = true)
        {
            var i = 0;            
            try
            {
                string querySku = sku;
                IPublishedContent retailer;
                var data = Umbraco.TypedContent(id);                
                try
                {
                    retailer = data.Children.FirstOrDefault(x => x.DocumentTypeAlias == "IndependentRetailers");
                }
                catch
                {
                    var res = new HttpResponseMessage(HttpStatusCode.NotFound);
                    return res;
                }
                var r = new List<Retailer>();
                
                var retailers = retailer.Children;

                if (includeElite)
                {
                    retailers = retailers.Where(retail => retail != null && retail.HasValue("brands") &&
                                    retail.GetPropertyValue<string>("brands").ToLower().Contains(brand.ToLower()));
                } 
                else
                {
                    retailers = retailers.Where(retail => retail != null && retail.HasValue("brands") &&
                                    retail.GetPropertyValue<string>("brands").ToLower().Contains(brand.ToLower()) &&
                                        retail.GetPropertyValue<bool>("isElite") == false);
                }

                if (range == "1")
                {
                    if (includeElite)
                    {
                        retailers =
                        retailers.Where(
                            x => x.GetPropertyValue<string>("catagories") != null && x.GetPropertyValue<string>("catagories").ToLower().Contains("range specialist"));
                    }
                    else
                    {
                        retailers =
                        retailers.Where(
                            x => x.GetPropertyValue<string>("catagories") != null && x.GetPropertyValue<string>("catagories").ToLower().Contains("range specialist") &&
                                x.GetPropertyValue<bool>("isElite") == false);
                    }
                }
                foreach (var retail in retailers)
                {
                    try
                    {
                        i++;
                        var temp = new Retailer
                        {
                            id = i,
                            name = retail.GetPropertyValue<string>("name"),
                            address = retail.GetPropertyValue<string>("address"),
                            email = retail.GetPropertyValue<string>("email"),
                            phone = retail.GetPropertyValue<string>("phoneNumber"),
                            web = retail.GetPropertyValue<string>("website"),
                            category =
                                retail.GetPropertyValue<bool>("isElite") ? "Elite Retailer" : "Independent Retailer",
                            isElite = retail.GetPropertyValue<bool>("isElite"),
                            local = "/retailers/" + retail.UrlName 
                        };

                        if (retail.HasValue("location"))
                        {
                            var latlng = retail.GetPropertyValue<string>("location").Split(',');
                            temp.lat = Convert.ToDouble(latlng[0]);
                            temp.lng = Convert.ToDouble(latlng[1]);
                        }

                        temp.categories = new List<Category>();

                        if (retail.HasValue("catagories"))
                        {
                            foreach (var item in retail.GetPropertyValue<string>("catagories").Split(','))
                            {
                                var cat = new Category {Name = item};
                                temp.categories.Add(cat);
                            }
                        }
                        if (!string.IsNullOrEmpty(querySku))
                        {
                            r.Add(temp);
                            //This is commented out for client as they do not have enough products assigned to retailers
                            /*var products = retail.GetPropertyValue<string>("products");
                            var bannerList =
                                products.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                            var productsCollection = Umbraco.TypedContent(bannerList).Where(x => x != null);

                            if (productsCollection.Any(x => x.Name == querySku || x.Children.Any(t => t.Name == querySku)))
                            {
                                r.Add(temp);
                            }*/
                        }
                        else
                        {
                            r.Add(temp);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error<Retailer>(string.Format("Problem with casting {0} to retailer", retail.Name), ex);

                    }
                }

                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(r))
                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch(Exception ex)
            {                
                LogHelper.Error<Retailer>("Problem with retailers", ex);
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
        }
    }
}
