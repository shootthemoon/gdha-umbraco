﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
// using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;
using Uniform.GDHA.Products;

namespace Uniform.ProductFilter
{
    [PluginController("Uniform")]
    public class ProductFilterApiController : UmbracoApiController
    {
        [HttpPost]
        public IEnumerable<ProductInfo> GetFilteredProductIds([FromUri] int id, [FromBody] FormDataCollection formData)
        {			
            var root = Umbraco.TypedContent(id);
            if (root == null)
                return null;
			var brand = root.AncestorOrSelf(1);
            var nodes = root.Descendants().Where(
                    x => x.DocumentTypeAlias == "ProductModel"
                );

            if (formData != null)
            {
                var filters = formData.ReadAsNameValueCollection();
                
                foreach (var filter in filters.AllKeys)
                {
                    var values = filters.GetValues(filter)
                        .Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                    if (values != null && values.Any())
                    {
                        switch (filter)
                        {
                            case "colour":
                                // multiple colours can be passed as multiple colour = nodeid 
                                nodes = nodes
                                    .Where(x => x.Children.Any(y => y.DocumentTypeAlias == "ProductSku"
                                    && values.Contains(y.GetPropertyValue<int>("productColour").ToString()))).Distinct();
                                break;
                            default:
                                // for features the query string pair is nodeId = optionId
                                nodes = nodes.Where(x =>
                                    ((ProductFeatureList)x.GetPropertyValue("productFeatures2"))
                                        .Items.Any(f => f.Id.ToString() == filter && f.Value != null && values.Contains(f.Value.ToString()))).Distinct();
                                break;
                        }
                    }
                }
            }

            //
            // you could use automapper for this. 
            //   but while you are chaning things around this lets you quickly see what you 
            //   are getting back, if you want something else, add it to the ProducInfo class
            //   and make sure it is set in this loop.
            var products = new List<ProductInfo>();
            foreach(var node in nodes)
            {
                var product = new ProductInfo()
                {
                    Id = node.Id,
                    Name = node.Name,
                    Description = node.GetPropertyValue<string>("bodyText"),
                    Url = node.Url,
                    Colours = new List<string>(),
					Sku = node.Children.First().Name,
					Reevoo = brand.GetPropertyValue<string>("reevooTrackingReference")
                };

                var images = node.Children.First().GetPropertyValue<string>("productImages");
                var imageIds = images.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var i = imageIds.Where(x => !Regex.IsMatch(x, @"[a-zA-Z]"));
                if (i.Any())
                {
                    var image = Umbraco.TypedMedia(i.First());
                    if (image != null)
                    {
                        product.ImgUrl = image.Url;
                    }
                }

                foreach(var sku in node.Children)
                {
                    if (sku.HasValue("productColour"))
                    {
                        var colourNode = Umbraco.TypedContent(sku.GetPropertyValue("ProductColour"));
                        if (colourNode != null)
                        {
                            var colour = colourNode.GetPropertyValue<string>("HexValue");
                            product.Colours.Add(colour);
                        }
                    }
                }
                products.Add(product);
            }
            return products;
        }


        public class ProductInfo
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Url { get; set; }
            public string ImgUrl { get; set; }
            public List<string> Colours { get; set; }
			public string Sku {get; set;}
			public string Reevoo {get;set;}
        }
    }

}
