﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net" %>

<script runat="server">
	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		this.ltrlLocalIpAddress.Text = this.GetLocalIpAddress();
		this.ltrlExternalIpAddress.Text = this.GetExternalIpAddress();
	}

	protected string GetLocalIpAddress()
	{
		var localIP = "?";
		
		try
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList)
			{
				if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
					localIP = ip.ToString();
			}
		}
		catch { }

		return localIP;
	}

	public string GetExternalIpAddress()
	{
		try
		{
			var request = WebRequest.Create("http://checkip.dyndns.org/");
			using (var response = request.GetResponse())
			using (var stream = new StreamReader(response.GetResponseStream()))
			{
				var contents = stream.ReadToEnd();
				var start = contents.IndexOf("Address: ") + 9;
				var end = contents.LastIndexOf("</body>");
				return contents.Substring(start, end - start);
			}
		}
		catch { }

		return string.Empty;
	}
</script>

<div class="">
    <h2>What Is My IP Address?</h2>
	<p>The IP addresses for this web-server are:</p>
	<ul>
		<li>
			<span>Local IP: </span>
			<asp:Literal runat="server" ID="ltrlLocalIpAddress" />
		</li>
		<li>
			<span>External IP: </span>
			<asp:Literal runat="server" ID="ltrlExternalIpAddress" />
		</li>
	</ul>

</div>
