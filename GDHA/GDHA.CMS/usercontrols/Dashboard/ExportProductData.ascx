﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="umbraco" %>
<%@ Import Namespace="Uniform.GDHA.BusinessLogic.Exports" %>
<%@ Register Assembly="controls" Namespace="umbraco.uicontrols" TagPrefix="umbraco" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var items = new List<ListItem>() { new ListItem(string.Concat(umbraco.ui.Text("choose"), "..."), string.Empty) };
            items.AddRange(umbraco.uQuery.GetNodesByType("USNHomepage").Select(x => new ListItem(x.Name, x.Id.ToString())));

            this.ddlBrands.Items.Clear();
            this.ddlBrands.Items.AddRange(items.ToArray());
        }
    }

    protected void btnExportProducts_Click(object sender, EventArgs e)
    {
        if (this.phFeedback.Visible)
            this.phFeedback.Visible = false;

        var nodeId = -1;
        if (!int.TryParse(this.ddlBrands.SelectedValue, out nodeId))
        {
            this.fbMessage.type = Feedback.feedbacktype.error;
            this.fbMessage.Text = "Please select a brand and try again.";

            this.phForm.Visible = true;
            this.phFeedback.Visible = true;
            return;
        }

        var homepage = uQuery.GetNode(nodeId);
        if (homepage == null)
            return;

        if (rblOptions.SelectedValue == "downloadVideo")
        {
            var tableTemp = new DataTable();
            tableTemp.Columns.Add("manufacturer", typeof(string));
            tableTemp.Columns.Add("Model", typeof(string));
            tableTemp.Columns.Add("sku", typeof(string));
            tableTemp.Columns.Add("name", typeof(string)); // max 255 chars
            tableTemp.Columns.Add("image_url", typeof(string)); // absolute URL (full size image)
            tableTemp.Columns.Add("product_category", typeof(string));
            tableTemp.Columns.Add("ean", typeof(string)); // optional
            tableTemp.Columns.Add("description", typeof(string)); // optional; max 32,000 chars
            tableTemp.Columns.Add("mpn", typeof(string)); // optional; same as sku for GDHA
            tableTemp.Columns.Add("colour", typeof(string)); // optional; colour of variant
            tableTemp.Columns.Add("Added",typeof(string));
            tableTemp.Columns.Add("Edited",typeof(string));
            tableTemp.Columns.Add("iSite_video_url",typeof(string));

            // get the products
            if (homepage != null)
            {
                var brandName = homepage.Name;

                var domain = library.NiceUrlWithDomain(homepage.Id).TrimEnd(new[] { '/' });
                var products = homepage.GetDescendantNodesByType("ProductModel");

                // loop through products
                foreach (var product in products)
                {
                    // loop through variants
                    foreach (var variant in product.GetChildNodesByType("ProductSku"))
                    {

                        var row = tableTemp.NewRow();

                        row["manufacturer"] = brandName;
                        row["Model"] = product.Name;
                        row["sku"] = variant.Name;
                        row["name"] = product.Name;
                        row["image_url"] = !string.IsNullOrEmpty(variant.GetProperty<string>("productImages")) ?
                                      !string.IsNullOrEmpty(uQuery.GetCsvIds(variant.GetProperty<string>("productImages")).FirstOrDefault())
                                      ? uQuery.GetMedia(uQuery.GetCsvIds(variant.GetProperty<string>("productImages")).FirstOrDefault()) != null
                                      ? !string.IsNullOrWhiteSpace(uQuery.GetMedia(uQuery.GetCsvIds(variant.GetProperty<string>("productImages")).FirstOrDefault()).GetImageUrl())
                                      ? string.Concat(domain, uQuery.GetMedia(uQuery.GetCsvIds(variant.GetProperty<string>("productImages")).FirstOrDefault()).GetImageUrl()).Contains(" ")
                                      //? Regex.Matches(string.Concat(domain, uQuery.GetMedia(uQuery.GetCsvIds(variant.GetProperty<string>("productImages")).FirstOrDefault()).GetImageUrl()).Replace("{src: '", "").Split(new [] { "', crops" }, StringSplitOptions.None)[0], @"(www.+|http.+)([\s]|$)")[0].Value
                                      ? string.Concat(domain, uQuery.GetMedia(uQuery.GetCsvIds(variant.GetProperty<string>("productImages")).FirstOrDefault()).GetImageUrl()).Replace("{src: '", "").Split(new [] { "', crops" }, StringSplitOptions.None)[0]
                                      
                                      : string.Concat(domain, uQuery.GetMedia(uQuery.GetCsvIds(variant.GetProperty<string>("productImages")).FirstOrDefault()).GetImageUrl())
                                      : string.Empty : string.Empty : string.Empty : string.Empty;


                        row["product_category"] = product.Parent.Name;
                        row["ean"] = variant.GetProperty<string>("productEAN");
                        row["description"] = product.GetProperty<string>("bodyText");
                        row["mpn"] = variant.Name;
                        row["colour"] = !string.IsNullOrEmpty(variant.GetProperty<string>("productColour"))
                            ? uQuery.GetNode(variant.GetProperty<string>("productColour")) != null
                                ? uQuery.GetNode(variant.GetProperty<string>("productColour")).Name : string.Empty : string.Empty;
                        row["added"] = product.CreateDate;
                        row["Edited"] = product.UpdateDate;
                        row["iSite_video_url"] = product.GetProperty<string>("productVideoUrl");
                        tableTemp.Rows.Add(row);
                    }
                }
            }
            var sb1  = Helper.ToCsv(tableTemp, ",");
            if (sb1.Length > 0)
            {
                Helper.DownloadCsv(string.Concat(homepage.Name, "-product-export_with_video_"), sb1);
            }
            else
            {
                this.fbMessage.type = Feedback.feedbacktype.error;
                this.fbMessage.Text = "There is no data to export. Please select a brand and try again.";

                this.phForm.Visible = true;
                this.phFeedback.Visible = true;
            }

            return;
        }

        var table = Products.GetProducts(homepage);
        var sb = Helper.ToCsv(table, ",");

        if (sb.Length > 0)
        {
            var brandName = homepage.Name;
            if (this.rblOptions.SelectedValue == "upload")
            {
                try
                {
                    if (Helper.UploadCsv(string.Concat(brandName, "-product-export", ".csv"), sb.ToString()))
                    {
                        this.fbMessage.type = Feedback.feedbacktype.success;
                        this.fbMessage.Text = "The products export was successfully uploaded";
                    }
                    else
                    {
                        this.fbMessage.type = Feedback.feedbacktype.notice;
                        this.fbMessage.Text = "There was a problem uploading the products export. Please try again, otherwise contact technical support.";
                    }
                }
                catch (Exception ex)
                {
                    this.fbMessage.type = Feedback.feedbacktype.error;
                    this.fbMessage.Text = string.Concat("Error: ", ex);
                }

                this.phForm.Visible = true;
                this.phFeedback.Visible = true;
            }
            else if (this.rblOptions.SelectedValue == "downloadVideo")
            {
                Helper.DownloadCsv(string.Concat(brandName, "-product-export_with_video_"), sb);
            }
            else
            {
                Helper.DownloadCsv(string.Concat(brandName, "-product-export_"), sb);
            }
        }
        else
        {
            this.fbMessage.type = Feedback.feedbacktype.error;
            this.fbMessage.Text = "There is no data to export. Please select a brand and try again.";

            this.phForm.Visible = true;
            this.phFeedback.Visible = true;
        }
    }

    protected void btnUploadCsv_Click(object sender, EventArgs e)
    {
        if (!this.fuCsv.HasFile)
        {
            this.fbMessage.type = Feedback.feedbacktype.error;
            this.fbMessage.Text = "There is no data to upload. Please select a file and try again.";
        }
        else
        {
            if (Helper.UploadCsv(this.fuCsv.PostedFile))
            {
                this.fbMessage.type = Feedback.feedbacktype.success;
                this.fbMessage.Text = "The products export was successfully uploaded";
            }
            else
            {
                this.fbMessage.type = Feedback.feedbacktype.notice;
                this.fbMessage.Text = "There was a problem uploading the products export. Please try again, otherwise contact technical support.";
            }
        }

        this.phForm.Visible = true;
        this.phFeedback.Visible = true;
    }
</script>

<div class="umb-pane ng-scope">
	<h2>Reevoo Product Data Export</h2>
	<img src="/usercontrols/Dashboard/reevoo-logo.png" alt="Reevoo Logo" class="dashboardIcon" />
    <br/>
	<asp:PlaceHolder runat="server" ID="phForm">
		<p>To export products for the Reevoo service, select a brand, then press the 'Export Products' button.</p>
		<div class="field">
			<asp:Label runat="server" AssociatedControlID="ddlBrands">Select brand: </asp:Label>
			<asp:DropDownList runat="server" ID="ddlBrands" />
			<asp:RadioButtonList runat="server" ID="rblOptions" CssClass="umb-radiobuttons" RepeatLayout="Flow" RepeatColumns="2" RepeatDirection="Horizontal">
				<%--<asp:ListItem Text="Download CSV" Value="download" Selected="true" />--%>
                <asp:ListItem Text="Download CSV with iSite" Value="downloadVideo" Selected="true" />
				<%--<asp:ListItem Text="Automatic Upload to Reevoo" Value="upload" />--%>
			</asp:RadioButtonList>
			<asp:Button runat="server" ID="btnExportProducts" Text="Export Products" OnClick="btnExportProducts_Click" />
		</div>
		<br />
		<div class="umb-property ng-scope" style="display: none;">
			<div class="control-group umb-control-group">
				<div class="">
					<h3>Manually Upload</h3>
					<p>You can manually upload a CSV export to the Reevoo secure FTP area:</p>
					<div class="field">
						<asp:FileUpload runat="server" ID="fuCsv" />
						<asp:Button runat="server" ID="btnUploadCsv" Text="Upload to Reevoo" OnClick="btnUploadCsv_Click" />
					</div>
				</div>
			</div>
		</div>
	</asp:PlaceHolder>

	<asp:PlaceHolder runat="server" ID="phFeedback" Visible="false">
		<div class="field">
			<umbraco:Feedback runat="server" ID="fbMessage" />
		</div>
	</asp:PlaceHolder>
</div>
