﻿angular.module('umbraco.resources').factory('uniformProductFeatureService',
    function ($q, $http) {

        return {
            getFeatures: function(nodeId)
            {
                return $http.get('backoffice/uniform/productFeatureApi/GetFeatures/' + nodeId);
            },
            listFeatures: function(nodeId, options)
            {
                var postVals = { id: nodeId, config: options };

                return $http({
                    method: 'POST',
                    url: 'backoffice/uniform/productFeatureApi/ListFeatures',
                    data: postVals
                });
            }
        }
    });