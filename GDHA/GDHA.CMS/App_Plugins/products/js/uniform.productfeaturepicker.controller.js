﻿angular.module("umbraco").controller("Uniform.ProductFeaturePicker",
    function ($scope, editorState, uniformProductFeatureService) {

        $scope.status = "Loading...";

        uniformProductFeatureService.listFeatures(editorState.current.id, $scope.model.config)
            .then(function (response) {

                $scope.status = response.data;

                angular.forEach($scope.status, function (set) {
                    angular.forEach(set.features, function (item) {

                        var modelValue = GetById($scope.model.value, item.Id);
                        console.log(modelValue);

                        if (modelValue > 0) {
                            console.log('Found');
                            item.Value = modelValue;
                        }
                    });

                });

            });

        var unsubscribe = $scope.$on("formSubmitting", function (ev, args) {
            console.log("submit - turn the features into the model?");

            var mVals = [];

            angular.forEach($scope.status, function (set) {
                angular.forEach(set.features, function (item) {
                    mVals.push({ Id: item.Id, Name: item.Name, Value: item.Value });
                });
            });

            $scope.model.value = mVals;
        });

        function GetById(arr, id) {

            var val = -1;
            angular.forEach(arr, function (item) {
                if (item.Id == id) {
                    val = item.Value;
                }
            });

            return val;
        }

        /* model = [{ Name: "", Value: ""},{Name: "", Value: ""}] */
    });