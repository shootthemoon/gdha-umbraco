$(function () {
	if ($('.open-2').length > 0) {
		$('.open-2').click(function () {
			if ($('.popup-active-2').length) {
				var sku = $('div.slideshow-2').data('FadeGallery').slides.filter('.active').attr('data-sku');
				$.get('GetProductPrices.ashx?sku=' + sku, function (data) {
					$('.popup-2').find('.holder').find('table').replaceWith(data);
				});
			}
		});
	}
});