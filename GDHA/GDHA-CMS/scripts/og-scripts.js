// $(document).ready(function() {
// 	$(".item:nth-child(odd), ul li:nth-child(odd), dl dt:nth-child(odd), dl dd:nth-child(odd), tr:nth-child(odd)").addClass("odd");
// 	$(".item:nth-child(even), ul li:nth-child(even), dl dt:nth-child(even), dl dd:nth-child(even), tr:nth-child(even)").addClass("even");
// 	// DROP DOWN - CLICK & HOVER - MAIN NAV
// 	$(".navigation nav.main ul li").hover(function(){ 
// 		$(this).toggleClass("hover"); 
// 		$(this).toggleClass("hover"); 
// 	});
// 	$(".navigation nav.main ul li > i").click(function(){
// 		if ($(".navigation nav.main ul li > i").length ) { 
// 			$(this).parent().toggleClass("open");
// 			$(this).parent().siblings().removeClass("open");
// 		}
// 		else{
// 			$(this).parent().toggleClass("open");
// 		}
// 	});
// 	 $("html").click(function() {
// 		$(".navigation nav.main ul li.open").removeClass("open");
// 	 });
// 	$(".navigation nav.main ul li > i").click(function(e){
// 		e.stopPropagation(); 
// 	});
// 	// EXPAND MOBILE NAVIVAGTION  
// 	$(".navigation a.expand").click(function(){
// 		if ($(".navigation .reveal").length ) { 
// 			$(".navigation a.expand").toggleClass('active');
// 			$(".navigation a.expand em").text('Close');
// 			$("html").toggleClass('reveal_out');
// 		}
// 		else{
// 			$(".navigation a.expand").toggleClass('active');
// 			$(".navigation a.expand em").text('Menu');
// 			$("html").toggleClass('reveal_out');
// 		}
// 	});
// 	// SLIDESHOWS
//      	$(".banner .slides").slick({
// 		arrows: true,
// 		dots: false,
// 		infinite: true,
// 		//speed: 200,
// 		fade: true,
//  			adaptiveHeight: true,
// 		prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left fa-3x"></i>',
// 		nextArrow: '<div class="slick-next"><i class="fa fa-angle-right fa-3x"></i>'
// 	});
//      	$(".slideshow").slick({
// 		arrows: true,
// 		dots: false,
// 		infinite: true,
// 		//speed: 200,
// 		fade: true,
//  			adaptiveHeight: true,
// 		prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left fa-2x"></i>',
// 		nextArrow: '<div class="slick-next"><i class="fa fa-angle-right fa-2x"></i>'
// 	});
// 	// BACK TO TOP
// 	if ( ($(window).height() + 100) < $(document).height() ) {
//         $('#top-link-block').addClass('show').affix({
//             // how far to scroll down before link "slides" into view
//             offset: {top:100}
//         });
// 	}
// });
// // OPEN MULTIPLE COLLAPSE PANELS
// // http://www.bootply.com/90JfjI2Q7n
// $(function () {	
// 	$('a[data-toggle="collapse"]').on('click',function(){
// 		var objectID=$(this).attr('href');
// 		if($(objectID).hasClass('in'))
// 		{
// 			$(objectID).collapse('hide');
// 		}
// 		else{
// 			$(objectID).collapse('show');
// 		}
// 	});
// 	// EXPAND ALL
// 	$('#expandAll').on('click',function(){
// 		$('a[data-toggle="collapse"]').each(function(){
// 			var objectID=$(this).attr('href');
// 			if($(objectID).hasClass('in')===false)
// 			{
// 				$(objectID).collapse('show');
// 			}
// 		});
// 	});
// 	// COLLAPSE ALL
// 	$('#collapseAll').on('click',function(){
// 		$('a[data-toggle="collapse"]').each(function(){
// 			var objectID=$(this).attr('href');
// 			$(objectID).collapse('hide');
// 		});
// 	});
// });
// // TRIGGER ANIMATIONS
// // http://www.oxygenna.com/tutorials/scroll-animations-using-waypoints-js-animate-css 
// $(window).bind('load resize', function(){
// 	function onScrollInit( items, trigger ) {
// 		items.each( function() {
// 			var osElement = $(this),
// 			osAnimationClass = osElement.attr('data-os-animation'),
// 			osAnimationDelay = osElement.attr('data-os-animation-delay');
// 			osElement.css({
// 				'-webkit-animation-delay':  osAnimationDelay,
// 				'-moz-animation-delay':     osAnimationDelay,
// 				'-ms-animation-delay':     osAnimationDelay,
// 				'animation-delay':          osAnimationDelay
// 			});
// 			var osTrigger = ( trigger ) ? trigger : osElement;
// 			osTrigger.waypoint(function() {
// 				osElement.addClass('animated').addClass(osAnimationClass);
// 			},{
// 				triggerOnce: true,
// 				offset: '80%'
// 			});
// 		});
// 	}
// 	onScrollInit( $('.os-animation') );
// });
// // LIGTHBOX
// $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
//    	event.preventDefault();
//        $(this).ekkoLightbox();
// }); 
// // MATCH HEIGHT OVERLAY TO IMAGE HEIGHT
// $(window).bind("load resize scroll",function(e){
// 	var img1 = $(".gallery img");
// 	$(".gallery .overlayicon").css({width:img1.width(), height:img1.height()});
// 	var img2 = $(".promo_pods .videopod img");
// 	$(".promo_pods .videopod .overlayicon").css({width:img2.width(), height:img2.height()});
// 	var img3 = $(".promo_pods .imagepod img");
// 	$(".promo_pods .imagepod .overlayicon").css({width:img3.width(), height:img3.height()});
// 	var img3 = $(".video-component .image.video img");
// 	$(".video-component .image.video .overlayicon").css({width:img3.width(), height:img3.height()});
// 	var img3 = $(".text-with-image_video .image.video img");
// 	$(".text-with-image_video .image.video .overlayicon").css({width:img3.width(), height:img3.height()});
// 	// VALIGN
// 	$('.banner .item:nth-child(1) .valign').valign();
// 	$('.banner .item:nth-child(2) .valign').valign();
// 	$('.banner .item:nth-child(3) .valign').valign();
// 	$('.banner .item:nth-child(4) .valign').valign();
// 	$('.banner .item:nth-child(5) .valign').valign();
// 	$('.banner .item:nth-child(6) .valign').valign();
// 	$('.banner .item:nth-child(7) .valign').valign();
// 	$('.banner .item:nth-child(8) .valign').valign();
// 	$('.banner .item:nth-child(9) .valign').valign();
// 	$('.banner .item:nth-child(10) .valign').valign();
// });
/* detect touch */
if ("ontouchstart" in window) {
    document.documentElement.className = document.documentElement.className + " touch";
}

if (!$("html").hasClass("touch")) {
    /* background fix */
    $(".parallax").css("background-attachment", "fixed");
}

/* fix vertical when not overflow
call fullscreenFix() if .fullscreen content changes */
function fullscreenFix() {
    var h = $("body").height();
    // set .fullscreen height
    $(".content-b").each(function(i) {
        if ($(this).innerHeight() > h) {
            $(this).closest(".fullscreen").addClass("overflow");
        }
    });
}

$(window).resize(fullscreenFix);

fullscreenFix();

/* resize background images */
function backgroundResize() {
    var windowH = $(window).height();
    $(".background").each(function(i) {
        var path = $(this);
        // variables
        var contW = path.width();
        var contH = path.height();
        var imgW = path.attr("data-img-width");
        var imgH = path.attr("data-img-height");
        var ratio = imgW / imgH;
        // overflowing difference
        var diff = parseFloat(path.attr("data-diff"));
        diff = diff ? diff : 0;
        // remaining height to have fullscreen image only on parallax
        var remainingH = 0;
        if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
            var maxH = contH > windowH ? contH : windowH;
            remainingH = windowH - contH;
        }
        // set img values depending on cont
        imgH = contH + remainingH + diff;
        imgW = imgH * ratio;
        // fix when too large
        if (contW > imgW) {
            imgW = contW;
            imgH = imgW / ratio;
        }
        //
        path.data("resized-imgW", imgW);
        path.data("resized-imgH", imgH);
        path.css("background-size", imgW + "px " + imgH + "px");
    });
}

$(window).resize(backgroundResize);

$(window).focus(backgroundResize);

backgroundResize();

/* set parallax background-position */
function parallaxPosition(e) {
    var heightWindow = $(window).height();
    var topWindow = $(window).scrollTop();
    var bottomWindow = topWindow + heightWindow;
    var currentWindow = (topWindow + bottomWindow) / 2;
    $(".parallax").each(function(i) {
        var path = $(this);
        var height = path.height();
        var top = path.offset().top;
        var bottom = top + height;
        // only when in range
        if (bottomWindow > top && topWindow < bottom) {
            var imgW = path.data("resized-imgW");
            var imgH = path.data("resized-imgH");
            // min when image touch top of window
            var min = 0;
            // max when image touch bottom of window
            var max = -imgH + heightWindow;
            // overflow changes parallax
            var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow;
            // fix height on overflow
            top = top - overflowH;
            bottom = bottom + overflowH;
            // value with linear interpolation
            var value = min + (max - min) * (currentWindow - top) / (bottom - top);
            // set background-position
            var orizontalPosition = path.attr("data-oriz-pos");
            orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
            $(this).css("background-position", orizontalPosition + " " + value + "px");
        }
    });
}

if (!$("html").hasClass("touch")) {
    $(window).resize(parallaxPosition);
    //$(window).focus(parallaxPosition);
    $(window).scroll(parallaxPosition);
    parallaxPosition();
}

$(function() {
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $(".dropdown").on("show.bs.dropdown", function(e) {
        $(this).find(".dropdown-menu").first().stop(true, true).slideDown();
    });
    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $(".dropdown").on("hide.bs.dropdown", function(e) {
        $(this).find(".dropdown-menu").first().stop(true, true).slideUp();
    });
    // back to top button
    // hide #back-top first
    $("#back-top").hide();
    // fade in #back-top
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $("#back-top").fadeIn();
        } else {
            $("#back-top").fadeOut();
        }
    });
    // scroll body to 0px on click
    $("#back-top a").click(function() {
        $("body,html").animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});

$(document).ready(function() {
    //affix search menu on mobiles
    $("#nav-affix").affix({
        offset: 50
    });
    $('[data-toggle="offcanvas"]').click(function() {
        $(".row-offcanvas").toggleClass("active");
    });
    listenWidth();
    // product buynow arrows
    $("#up").click(function() {
        $("#buynow_menu").animate({
            scrollTop: "-=56px"
        });
    });
    $("#down").click(function() {
        $("#buynow_menu").animate({
            scrollTop: "+=56px"
        });
    });
    //product gallery script
    $("#thumbnails a").on("click", function(e) {
        e.preventDefault();
        $("#big").attr("src", $(this).attr("target"));
    });
    //svg onhover change stroke colours  
    function changeColourSvg(className) {
        $("#" + className).hover(function() {
            /* Stuff to do when the mouse enters the element */
            $("." + className).attr("class", className + " active");
            $("." + className + "-circle").attr("class", className + "-circle active");
            $("." + className + "-text").attr("class", className + "-text active");
        }, function() {
            /* Stuff to do when the mouse leaves the element */
            $("." + className).attr("class", className);
            $("." + className + "-circle").attr("class", className + "-circle");
            $("." + className + "-text").attr("class", className + "-text");
        });
        $("#" + className).click(function(event) {
            /* Act on the event */
            //remove active class from all elements
            $(".svg-1, .svg-2, .svg-3, .svg-4").css({
                stroke: "#c6c6c6"
            });
            $(".svg-1-circle, .svg-2-circle, .svg-3-circle, .svg-4-circle").css({
                fill: "#C6C6C6"
            });
            $(".svg-1-text, .svg-2-text, .svg-3-text, .svg-4-text").css({
                fill: "#000"
            });
            //add active class to target element
            $("." + className).css({
                stroke: "#3F1F10"
            });
            $("." + className + "-circle").css({
                fill: "#3F1F10"
            });
            $("." + className + "-text").css({
                fill: "#FFF"
            });
        });
    }
    // set functions for all four svg types. 
    changeColourSvg("svg-1");
    changeColourSvg("svg-2");
    changeColourSvg("svg-3");
    changeColourSvg("svg-4");
    // section for product features
    $("#feature-collapse").hide();
    $("body").on("click", "#feature-close", function() {
        $(".feature-control").removeClass("active");
    });
    $(".feature-control").click(function(e) {
        /* Act on the event */
        e.preventDefault();
        var prdName = $(this).data("name");
        var prdImg = $(this).data("img-src");
        var prdText = $(this).data("text");
        $(".feature-control").removeClass("active");
        $(this).addClass("active");
        // function scrollToAnchor(aid){
        //   var aTag = $("div[name='"+ aid +"']");
        //   $('html,body').animate({scrollTop: aTag.offset().top},'slow');
        // }
        // //scroll to section
        // scrollToAnchor('features-collapse');
        $("#feature-collapse").slideDown("fast", function() {
            $("#feature-img").attr("src", prdImg);
            $("#feature-name").html(prdName);
            $("#feature-text").html(prdText);
        });
    });
    $("#feature-close").click(function(e) {
        /* Act on the event */
        $("#feature-collapse").slideUp("fast");
    });
    // onclick hide other menus
    $("#pr-click").click(function() {
        /* Act on the event */
        $("#whybuy").removeClass("in");
        $("#search-nav").removeClass("in");
    });
    $("#wb-click").click(function() {
        /* Act on the event */
        $("#categories").removeClass("in");
        $("#search-nav").removeClass("in");
    });
    $("#se-click").click(function() {
        /* Act on the event */
        $("#categories").removeClass("in");
        $("#whybuy").removeClass("in");
        $("#search_field").focus();
    });
    // Product filter, onclick toggle bold selector
    $(".checkbox input").click(function() {
        /* Act on the event */
        $(this).parent("label").toggleClass("selected");
    });
    $(".carousel").carousel({
        interval: 5e3
    });
    // on click toggle chevron option
    $(".collapse-arrow").click(function() {
        /* Act on the event */
        $(this).find(".arr").toggleClass("rotate-arrow");
    });
});

// smooth scroll to anchor
$(function() {
    $('.anchor a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate({
                    scrollTop: target.offset().top
                }, 1e3);
                return false;
            }
        }
    });
});

$(document).load($(window).bind("resize", listenWidth));

// re-order page content on product page
function listenWidth(e) {
    if ($(window).width() < 690) {
        $("#product-info").remove().insertAfter($("#product-image"));
    } else {
        $("#product-info").remove().insertBefore($("#product-image"));
    }
}

/*
 * Replace all SVG images with inline SVG
 */
jQuery("img.svg").each(function() {
    var $img = jQuery(this);
    var imgID = $img.attr("id");
    var imgClass = $img.attr("class");
    var imgURL = $img.attr("src");
    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find("svg");
        // Add replaced image's ID to the new SVG
        if (typeof imgID !== "undefined") {
            $svg = $svg.attr("id", imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== "undefined") {
            $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr("xmlns:a");
        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
            $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
        }
        // Replace image with new SVG
        $img.replaceWith($svg);
    }, "xml");
});