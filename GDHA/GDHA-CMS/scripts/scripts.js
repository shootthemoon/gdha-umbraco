/* detect touch */
if ("ontouchstart" in window) {
    document.documentElement.className = document.documentElement.className + " touch";
}

if (!$("html").hasClass("touch")) {
    /* background fix */
    $(".parallax").css("background-attachment", "fixed");
}

/* fix vertical when not overflow
call fullscreenFix() if .fullscreen content changes */
function fullscreenFix() {
    var h = $("body").height();
    // set .fullscreen height
    $(".content-b").each(function(i) {
        if ($(this).innerHeight() > h) {
            $(this).closest(".fullscreen").addClass("overflow");
        }
    });
}

$(window).resize(fullscreenFix);

fullscreenFix();

/* resize background images */
function backgroundResize() {
    var windowH = $(window).height();
    $(".background").each(function(i) {
        var path = $(this);
        // variables
        var contW = path.width();
        var contH = path.height();
        var imgW = path.attr("data-img-width");
        var imgH = path.attr("data-img-height");
        var ratio = imgW / imgH;
        // overflowing difference
        var diff = parseFloat(path.attr("data-diff"));
        diff = diff ? diff : 0;
        // remaining height to have fullscreen image only on parallax
        var remainingH = 0;
        if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
            var maxH = contH > windowH ? contH : windowH;
            remainingH = windowH - contH;
        }
        // set img values depending on cont
        imgH = contH + remainingH + diff;
        imgW = imgH * ratio;
        // fix when too large
        if (contW > imgW) {
            imgW = contW;
            imgH = imgW / ratio;
        }
        //
        path.data("resized-imgW", imgW);
        path.data("resized-imgH", imgH);
        path.css("background-size", imgW + "px " + imgH + "px");
    });
}

$(window).resize(backgroundResize);

$(window).focus(backgroundResize);

backgroundResize();

/* set parallax background-position */
function parallaxPosition(e) {
    var heightWindow = $(window).height();
    var topWindow = $(window).scrollTop();
    var bottomWindow = topWindow + heightWindow;
    var currentWindow = (topWindow + bottomWindow) / 2;
    $(".parallax").each(function(i) {
        var path = $(this);
        var height = path.height();
        var top = path.offset().top;
        var bottom = top + height;
        // only when in range
        if (bottomWindow > top && topWindow < bottom) {
            var imgW = path.data("resized-imgW");
            var imgH = path.data("resized-imgH");
            // min when image touch top of window
            var min = 0;
            // max when image touch bottom of window
            var max = -imgH + heightWindow;
            // overflow changes parallax
            var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow;
            // fix height on overflow
            top = top - overflowH;
            bottom = bottom + overflowH;
            // value with linear interpolation
            var value = min + (max - min) * (currentWindow - top) / (bottom - top);
            // set background-position
            var orizontalPosition = path.attr("data-oriz-pos");
            orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
            $(this).css("background-position", orizontalPosition + " " + value + "px");
        }
    });
}

if (!$("html").hasClass("touch")) {
    $(window).resize(parallaxPosition);
    //$(window).focus(parallaxPosition);
    $(window).scroll(parallaxPosition);
    parallaxPosition();
}

$(function() {
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $(".dropdown").on("show.bs.dropdown", function(e) {
        $(this).find(".dropdown-menu").first().stop(true, true).slideDown();
    });
    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $(".dropdown").on("hide.bs.dropdown", function(e) {
        $(this).find(".dropdown-menu").first().stop(true, true).slideUp();
    });
    // back to top button
    // hide #back-top first
    $("#back-top").hide();
    // scroll body to 0px on click
    $("#back-top a").click(function() {
        $("body,html").animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});

// fade in #back-top
$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        $("#back-top").fadeIn();
    } else {
        $("#back-top").fadeOut();
    }
});

$(document).ready(function() {
    //affix search menu on mobiles
    $("#nav-affix").affix({
        offset: 50
    });
    $('[data-toggle="offcanvas"]').click(function() {
        $(".row-offcanvas").toggleClass("active");
    });
    listenWidth();
    // product buynow arrows
    $("#up").click(function() {
        $("#buynow_menu").animate({
            scrollTop: "-=56px"
        });
    });
    $("#down").click(function() {
        $("#buynow_menu").animate({
            scrollTop: "+=56px"
        });
    });
    //product gallery script
    $("#thumbnails a").on("click", function(e) {
        e.preventDefault();
        $("#big").attr("src", $(this).attr("target"));
    });
    // section for product features
    $("#feature-collapse").hide();
    $("body").on("click", "#feature-close", function() {
        $(".feature-control").removeClass("active");
    });
    $(".feature-control").click(function(e) {
        /* Act on the event */
        e.preventDefault();
        var prdName = $(this).data("name");
        var prdImg = $(this).data("img-src");
        var prdText = $(this).data("text");
        $(".feature-control").removeClass("active");
        $(this).addClass("active");
        $("#feature-collapse").slideDown("fast", function() {
            $("#feature-img").attr("src", prdImg);
            $("#feature-name").html(prdName);
            $("#feature-text").html(prdText);
        });
    });
    $("#feature-close").click(function(e) {
        /* Act on the event */
        $("#feature-collapse").slideUp("fast");
    });
    // onclick hide other menus
    $("#pr-click").click(function() {
        /* Act on the event */
        $("#whybuy").removeClass("in");
        $("#search-nav").removeClass("in");
    });
    $("#wb-click").click(function() {
        /* Act on the event */
        $("#categories").removeClass("in");
        $("#search-nav").removeClass("in");
    });
    $("#se-click").click(function() {
        /* Act on the event */
        $("#categories").removeClass("in");
        $("#whybuy").removeClass("in");
        $("#search_field input").focus();
    });
    // Product filter, onclick toggle bold selector
    $(".checkbox input").click(function() {
        /* Act on the event */
        $(this).parent("label").toggleClass("selected");
    });
    $(".carousel").carousel({
        interval: 5e3
    });
    // on click toggle chevron option
    $(".collapse-arrow").click(function() {
        /* Act on the event */
        $(this).find(".arr").toggleClass("rotate-arrow");
    });
});

// smooth scroll to anchor
$(function() {
    $('.anchor a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate({
                    scrollTop: target.offset().top
                }, 1e3);
                return false;
            }
        }
    });
	
	$('.show-specification').on('click', function(){
		$('#collapseSpecification').collapse('show');
	});
	
});

$(document).load($(window).bind("resize", listenWidth));

// re-order page content on product page
function listenWidth(e) {
    if ($(window).width() < 690) {
        $("#product-info").remove().insertAfter($("#product-image"));
    } else {
        $("#product-info").remove().insertBefore($("#product-image"));
    }
}

/*
 * Replace all SVG images with inline SVG
 */
jQuery("img.svg").each(function() {
    var $img = jQuery(this);
    var imgID = $img.attr("id");
    var imgClass = $img.attr("class");
    var imgURL = $img.attr("src");
    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find("svg");
        // Add replaced image's ID to the new SVG
        if (typeof imgID !== "undefined") {
            $svg = $svg.attr("id", imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== "undefined") {
            $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr("xmlns:a");
        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
            $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
        }
        // Replace image with new SVG
        $img.replaceWith($svg);
    }, "xml");
});


$(function() {
	$('body').on('click', '#buynow_menu a', function(){
		var title = $(this).attr('title');
		dataLayer.push({'retailerTitle': title});
	});
});




