﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductImportMigrationDashboard.ascx.cs" Inherits="Uniform.GDHA.ProductImport.ProductImportMigrationDashboard" %>
<script type="text/javascript">
    $(document.forms[0]).submit(function () {
        document.getElementById("progressnote").innerHTML
            = "Doing stuff... <small>can take a little while</small>";

        document.getElementById("uniformprogress").style.visibility = "visible";
    });
</script>

<div class="row">
    <div class="span6">
        <h2>1. Import Media Tree</h2>
        <p>
            will import the media settings and copy files from an existing 
            media tree..
        </p>
        <div class="form">
            <div class="form-group">
                <label for="tbMediaConfig" runat="server">Media Config Folder</label>
                <asp:TextBox ID="tbMediaConfig" runat="server" CssClass="form-control"></asp:TextBox>
            </div>

            <div class="form-group">
                <label for="tbMediaSource" runat="server">Media Source Folder</label>
                <asp:TextBox ID="tbMediaSource" runat="server" CssClass="form-control"></asp:TextBox>
            </div>

            <div class="form-group">
                <asp:Button Id="btnMediaImport" runat="server" Text="Import Media" CssClass="btn btn-danger" OnClick="btnMediaImport_Click" />
            </div>
        </div>
    </div>

    <div class="span6">
        <h2>2. Product Importer</h2>
        <p>
            Will import a product tree to the location you choose.
        </p>
        <div class="form">
            <div class="form-group">
                <label for="tbFolderLocation" runat="server">Folder</label>
                <asp:TextBox ID="tbFolderLocation" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="">Parent Id</label>
                <asp:TextBox ID="tbParentId" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Button ID="btnImport" runat="server" text="Import" OnClick="btnImport_Click"/>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="span12">
        <div>
            <asp:Label ID="lbErrors" runat="server" CssClass="text-danger"></asp:Label>
        </div>
        <div>
           <asp:Label ID="lbStatus" runat="server"></asp:Label>
        </div>
    </div>
</div>

<div id="uniformprogress" style="visibility:hidden;">
    <div class="row">
        <div class="span12">
            <h3 id="progressnote"></h3>
            <div class="progress progress-striped active">
                <div class="bar" style="width: 100%;"></div>
            </div>
        </div>
    </div>
</div>
