﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using GoogleMapsApi;
using GoogleMapsApi.Entities.Geocoding.Request;
using umbraco;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Uniform.Web.Models;

namespace Uniform.Web.SurfaceControllers
{
	public class BrandBrochureOrderSurfaceController : SurfaceController
	{
		[HttpPost]
        public ActionResult SendBrochureForm(BrandBrochureOrderModel model)
		{
			if (!ModelState.IsValid)
				return CurrentUmbracoPage();

            IEnumerable<IPublishedContent> rootNode = Umbraco.TypedContentAtRoot();
            var addresses = new List<RetailerAddress>();
		    var brand = CurrentPage.AncestorOrSelf(1).Name;
            var retailers = rootNode.First(x => x.DocumentTypeAlias == "ConfigFolder").FirstChild(x => x.DocumentTypeAlias == "IndependentRetailers");
            List<Retailer> r = new List<Retailer> { };
            int i = 0;

            

            var poiRequest = new GeocodingRequest()
            {
                Address = string.Format("{0}, {1}, {2}, {3}", model.Street, model.Town, model.County, model.Postcode),
            };
            var poi = GoogleMaps.Geocode.Query(poiRequest);            
            var homeLat = poi.Results.Any() ? poi.Results.First().Geometry.Location.Latitude: 0.00;
		    var homeLng = poi.Results.Any() ? poi.Results.First().Geometry.Location.Longitude: 0.00;
            var list =  new List<RetailerAddress>();
            foreach (var retail in retailers.Children)
            {
                if (retail.HasValue("brands") && retail.GetPropertyValue<string>("brands").ToLower().Contains(brand))
                {
                    i++;
                    var temp = new Retailer();

                    temp.id = i;
                    temp.name = retail.Name;
                    temp.address = retail.GetPropertyValue<string>("address");
                    temp.email = retail.GetPropertyValue<string>("email");
                    if (retail.HasValue("location"))
                    {
                        var latlng = retail.GetPropertyValue<string>("location").Split(',');
                        temp.lat = Convert.ToDouble(latlng[0]);
                        temp.lng = Convert.ToDouble(latlng[1]);
                    }
                    temp.phone = retail.GetPropertyValue<string>("phoneNumber");
                    temp.web = retail.GetPropertyValue<string>("website");
                    temp.categories = new List<Category> { };

                    if (retail.HasValue("categories"))
                    {
                        foreach (var item in retail.GetPropertyValue<string>("categories").Split(','))
                        {
                            var cat = new Category();
                            cat.Name = item;
                            temp.categories.Add(cat);
                        }
                    }
                    r.Add(temp);
                }
            }
            foreach (var node in r)
		    {
		        var address = node.address;
		        var email = node.email;
                var url = node.web;
                var name = node.name;
                var phone = node.phone;
		        var cat = "";
                if (node.categories != null)
		        {
		            foreach (var ct in node.categories)
		            {
		                var catName = ct.Name;
		                if (!String.IsNullOrEmpty(catName))
		                {
		                    cat += catName + ",";
		                }
		            }
		        }

                var lat = node.lat;
                var lng = node.lng;

                if (node.lat == 0 && node.lng == 0)
		        {
		            var geocodeRequest = new GeocodingRequest()
		            {
		                Address = address,
		            };
		            var geocode = GoogleMaps.Geocode.Query(geocodeRequest);
		            lat = geocode.Results.Any() ? geocode.Results.First().Geometry.Location.Latitude : 0.00;
		            lng = geocode.Results.Any() ? geocode.Results.First().Geometry.Location.Longitude : 0.00;
		        }
		        var distance = DistanceBetweenLocations(homeLat,homeLng,lat,lng) * 0.000621371192; // Converts metres to miles 0.000621371192 miles = 1 metre
                RetailerAddress ad =  new RetailerAddress()
                {
                    Address1 = address,
                    Email = email,
                    Telephone = phone,
                    Name = name,
                    WebsiteUrl = url,
                    Lat = lat.ToString(CultureInfo.InvariantCulture),
                    Lng = lng.ToString(CultureInfo.InvariantCulture),
                    RetailerCategories = cat,
                    MilesAway = distance //assign the distance in metres for the sake of the order by 
                };
		        
                if (ad.MilesAway < 75) //75 miles 
                {      
		            list.Add(ad);
                }
		    }            

            if (list.Count > 0)
            {            
                var geocodes = list.OrderBy(x => x.MilesAway).Take(4);

                foreach (var address in geocodes)
                {
                    address.DistanceAway = Math.Round(address.MilesAway, 2).ToString(CultureInfo.InvariantCulture);

                    if (!string.IsNullOrWhiteSpace(address.RetailerCategories))
                    {
                        //address.IsElite = categories.Any(x => x.GetProperty<bool>("isElite"));// Needs to be in umbraco
                    }
                    addresses.Add(address);
                }
            }
            
			// process the form
			var emailFrom = CurrentPage.GetPropertyValue<string>("emailFrom") ?? "no-reply@localhost";
			var emailTo = CurrentPage.GetPropertyValue<string>("emailTo");
			var emailSubject = CurrentPage.GetPropertyValue<string>("emailSubject");
			var emailBody = CurrentPage.GetPropertyValue<string>("emailBody");

			if (!string.IsNullOrEmpty(emailTo) && !string.IsNullOrEmpty(emailSubject))
			{
				// process the email body text
                emailBody = ReplaceShortcodes(emailBody, model);

				// send the email
				library.SendMail(emailFrom, emailTo, emailSubject, emailBody, false);

				// store data in CSV
                LogContactForm(model, addresses);
			}

			// redirect to the "thank you" page.
			return RedirectToUmbracoPage(CurrentPage.Children.Any()? CurrentPage.Children.First() : CurrentPage);
		}

        private string ReplaceShortcodes(string content, BrandBrochureOrderModel model)
		{
            content = content.Replace("{Title}", model.Title);
            content = content.Replace("{FirstName}", model.FirstName);
            content = content.Replace("{LastName}", model.LastName);
            content = content.Replace("{Email}", model.Email);
            content = content.Replace("{Telephone}", model.Telephone);
            content = content.Replace("{PropertyNumber}", model.PropertyNumber);
            content = content.Replace("{Street}", model.Street);
            content = content.Replace("{Town}", model.Town);
            content = content.Replace("{County}", model.County);
            content = content.Replace("{Postcode}", model.Postcode);
            content = content.Replace("{Brochures}", string.Join("|", model.PostedBrochurers.BrochureIDs));
            content = content.Replace("{GivenConsentToBeContacted}", model.GivenConsentToBeContacted.ToString());

			return content;}

        private double DistanceBetweenLocations(double latA, double lngA, double latB, double lngB)
	    {
            var locA = new GeoCoordinate(latA, lngA);
            var locB = new GeoCoordinate(latB, lngB);
            var distance = locA.GetDistanceTo(locB);
            return distance;
	    }

        private void LogContactForm(BrandBrochureOrderModel model, List<RetailerAddress> addresses)
		{
			try
			{
				var path = IOHelper.MapPath("~/App_Data/TEMP/FormData/BrochureOrders");

				if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                var filePath = string.Format("{0}/{1}-brochure-form.csv", path, DateTime.Today.ToString("yyyy-MM-dd"));

                var addHeader = !System.IO.File.Exists(filePath);

                using (var sw = System.IO.File.AppendText(filePath))
				{
                    if(addHeader)
                    {
                        var header = new List<string>
                        {
                            "Title",
                            "FirstName",
                            "LastName",
                            "Email",
                            "Telephone",
                            "PropertyNumber",
                            "Street",
                            "Town",
                            "County",
                            "Postcode",
                            "Brochures",
                            "GivenConsentToBeContacted"
                        };


                        for (var i = 1; i <= 4; i++)
                        {
                            header.Add("Name_" + i);
                            header.Add("Address1_" + i);
                            header.Add("Address2_" + i);
                            header.Add("Locality_" + i);
                            header.Add("Region_" + i);
                            header.Add("PostalCode_" + i);
                            header.Add("Telephone_" + i);
                            header.Add("Fax_" + i);
                            header.Add("Email_" + i);
                            header.Add("WebsiteUrl_" + i);
                        }

                        sw.WriteLine("\"" + string.Join("\",\"", header.ToArray()) + "\"");
                    }

				    var values = new List<string>
				    {
				        model.Title,
				        model.FirstName,
				        model.LastName,
				        model.Email,
				        model.Telephone,
				        model.PropertyNumber,
				        model.Street,
				        model.Town,
				        model.County,
				        model.Postcode,
				        string.Join("|", model.PostedBrochurers.BrochureIDs),
				        model.GivenConsentToBeContacted.ToString()
				    };


				    foreach (var address in addresses)
                    {
                        values.Add(address.Name);
                        values.Add(address.Address1);
                        values.Add(address.Address2);
                        values.Add(address.Locality);
                        values.Add(address.Region);
                        values.Add(address.PostalCode);
                        values.Add(address.Telephone);
                        values.Add(address.Fax);
                        values.Add(address.Email);
                        values.Add(address.WebsiteUrl);
                    }

					sw.WriteLine("\"" + string.Join("\",\"", values.ToArray()) + "\"");
				}

			}
			catch (Exception ex)
			{
				LogHelper.Error<BrandBrochureOrderSurfaceController>("There was an error logging the contact form data.", ex);
			}
		}
	}
}