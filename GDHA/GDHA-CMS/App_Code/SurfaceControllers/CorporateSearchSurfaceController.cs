﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Examine;
using Examine.LuceneEngine.SearchCriteria;
using Examine.SearchCriteria;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using Uniform.Web.Models;

namespace Uniform.Web.SurfaceControllers
{
    public sealed class CorporateSearchSurfaceController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult Search(string query)
        {
            if (!string.IsNullOrEmpty(query) || !string.IsNullOrWhiteSpace(query))
            {
                var searchCriteria = ExamineManager.Instance.CreateSearchCriteria()
                    .Field("content", query.Fuzzy(0.8f))
                    .Compile();

                return PartialView("~/Views/Partials/GDHA/CorporateSearch.cshtml", new SearchModel
                {
                    Query = query,
                    SearchResults = Umbraco.TypedSearch(searchCriteria)
                });
            }

            return PartialView("~/Views/Partials/GDHA/CorporateSearch.cshtml", new SearchModel
            {
                Query = query,
                SearchResults = new List<IPublishedContent>()
            });
        }

        [ChildActionOnly]
        public ActionResult SearchPress(string query, string brandsSelected, string yearsSelected)
        {

            IBooleanOperation criteria = null;

            if (!string.IsNullOrEmpty(query) || !string.IsNullOrWhiteSpace(query))
            {
                criteria = ExamineManager.Instance.CreateSearchCriteria().NodeTypeAlias("PressRelease").And()
                    .Field("content", query.Fuzzy(0.8f));
            }
            else
            {
                criteria = ExamineManager.Instance.CreateSearchCriteria().NodeTypeAlias("PressRelease");
            }

            if (!string.IsNullOrEmpty(brandsSelected))
            {
                string[] brands = brandsSelected.Split(',');

                criteria = criteria.And().GroupedOr(new string[] { "relatedToBrand" }, brands);
            }

            if (!string.IsNullOrEmpty(yearsSelected))
            {
                string[] years = yearsSelected.Split(',');

                criteria = criteria.And().GroupedOr(new string[] { "publishedDate" }, years.Select(s => s.MultipleCharacterWildcard()).ToArray());
            }

            var results = Umbraco.TypedSearch(criteria.Compile());

            return PartialView("~/Views/Partials/GDHA/CorporateSearchPress.cshtml", new SearchModel
            {
                Query = query,
                SearchResults = results
            });
        }
    }
}
