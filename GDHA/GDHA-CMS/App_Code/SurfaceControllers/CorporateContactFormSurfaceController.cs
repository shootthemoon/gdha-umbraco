﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Uniform.Web.Models;

namespace Uniform.Web.SurfaceControllers
{
    public sealed class CorporateContactFormSurfaceController : SurfaceController
    {
        [HttpPost]
        public ActionResult SendContactForm(CorpContactFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            // process the form
            var isHtml = false;
            var emailFrom = this.CurrentPage.GetPropertyValue<string>("emailFrom") ?? "no-reply@localhost";
            var emailTo = this.CurrentPage.GetPropertyValue<string>("emailTo");
            var emailSubject = this.CurrentPage.GetPropertyValue<string>("emailSubject");
            var emailBody = this.CurrentPage.GetPropertyValue<string>("emailBody");

            if (!string.IsNullOrEmpty(emailTo) && !string.IsNullOrEmpty(emailSubject))
            {
                // process the email body text
                emailBody = this.ReplaceShortcodes(emailBody, Request.Form);

                // send the email
                umbraco.library.SendMail(emailFrom, emailTo, emailSubject, emailBody, isHtml);

                // store data in CSV
                this.LogContactForm(this.CurrentPage.Id, Request.Form);
            }

            return RedirectToUmbracoPage(this.CurrentPage.Children.FirstOrDefault());
        }

        private string ReplaceShortcodes(string content, NameValueCollection replacements)
        {
            foreach (string key in replacements.AllKeys)
            {
                var shortcode = string.Concat('{', key, '}');
                content = content.Replace(shortcode, replacements[key]);
            }

            return content;
        }

        private void LogContactForm(int nodeId, NameValueCollection form)
        {
            try
            {
                var path = IOHelper.MapPath("~/App_Data/TEMP/FormData");

                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);

                using (var sw = System.IO.File.AppendText(string.Format("{0}/gdha-contact-form-{1}.csv", path, nodeId)))
                {
                    var values = new List<string>();

                    foreach (string key in form.AllKeys.Where(x => x != "uformpostroutevals"))
                    {
                        values.Add(form[key]);
                    }

                    sw.WriteLine("\"" + string.Join("\",\"", values.ToArray()) + "\"");
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error<CorporateContactFormSurfaceController>("There was an error logging the contact form data.", ex);
            }
        }
    }
}
