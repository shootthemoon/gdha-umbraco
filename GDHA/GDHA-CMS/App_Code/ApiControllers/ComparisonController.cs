﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Uniform.GDHA.Products;

namespace Uniform.Amtico
{
    public class ComparisonController : UmbracoApiController
    {
        [HttpPost]
        public async Task<HttpResponseMessage> Products(HttpRequestMessage request)
        {
            var jsonString = await request.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<ComparisonData>(jsonString);
            var results = new ComparisonResults();

            var products = Umbraco.TypedContent(model.products);
           

            results.count = products.Count();
            var blanks = 4 - results.count;


            foreach (var product in products)
            {
                var imageList = product.Children.First().GetPropertyValue<string>("productImages");
                char[] splitChar = { ',' };
                string[] ids = imageList.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
                var image = Umbraco.Media(ids.First());
                results.products.Add(new ProductInfo()
                {
                    empty = false,
                    id = product.Id,
                    name = product.Name,                                        
                    swatch = image.Url,
                    url = product.Url
                });
            }
            if (products.Any())
            {

                var groups = GetGroups(products.First().Id);

                for (int n = 0; n < blanks; n++)
                {
                    results.products.Add(new ProductInfo()
                    {
                        empty = true
                    });
                }



                results.comparisons.Add(GetProductCodes(products, blanks));

                foreach (var group in groups)
                {
                    var comparison = new ComparisonSet();
                    comparison.name = group.Name;

                    foreach (var product in products)
                    {
                        var vals = GetComparisonData(product, group, model.Culture);
                        comparison.products.Add(new ProductData()
                        {
                            empty = !vals.Any(),
                            values = vals
                        });
                    }

                    comparison.products.AddRange(GetBlanks(blanks));

                    results.comparisons.Add(comparison);
                }

                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(results))
                };

                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            // we just return 4 blanks...
            return new HttpResponseMessage(HttpStatusCode.Ambiguous);
        }

        private IEnumerable<IPublishedContent> GetGroups(int i)
        {
            var settings = Umbraco.TypedContent(i);
            var compIds = settings.GetPropertyValue<string>("productKeyFeatures", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var groupNodes = Umbraco.TypedContent(compIds);
            return groupNodes;

        }

        private ComparisonSet GetProductCodes(IEnumerable<IPublishedContent> products, int blanks)
        {
            // get product codes
            var codes = new ComparisonSet();
            codes.name = "Sku";

            foreach (var product in products)
            {
                codes.products.Add(new ProductData()
                {
                    values = new List<string>() { product.Children.FirstOrDefault().Name }
                });
            }

            codes.products.AddRange(GetBlanks(blanks));

            return codes;
        }


        private IEnumerable<ProductData> GetBlanks(int count)
        {
            List<ProductData> blanks = new List<ProductData>();
            for (int n = 0; n < count; n++)
            {
                blanks.Add(new ProductData()
                {
                    empty = true
                });
            }

            return blanks;
        }


        private List<string> GetComparisonData(IPublishedContent product, IPublishedContent group, string culture)
        {
            List<string> values = new List<string>();


            var keyListValue = product.GetPropertyValue<string>("ProductKeyFeatures");
            var keyList = keyListValue.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            var nodeIds = new int[200];
            for (var runs = 0; runs < keyList.Count(); runs++)
            {
                int number;
                bool result = Int32.TryParse(keyList[runs], out number);
                if (result)
                {
                    nodeIds[runs] = number;
                }
            }

            var keyCollection = Umbraco.TypedContent(nodeIds).Where(x => x.Name == group.Name);
            values.Add(keyCollection.Any() ? "Yes" : "No");


            return values;
        }
    }

    public class ComparisonData
    {
        public IEnumerable<int> products { get; set; }
        public string Culture { get; set; }
    }

    public class ComparisonResults
    {
        public ComparisonResults()
        {
            products = new List<ProductInfo>();
            comparisons = new List<ComparisonSet>();

        }

        public int count { get; set; }
        public List<ProductInfo> products;
        public List<ComparisonSet> comparisons;
    }

    public class ComparisonSet
    {
        public ComparisonSet()
        {
            products = new List<ProductData>();
        }

        public string name { get; set; }

        public List<ProductData> products;
    }

    public class ProductInfo
    {
        public bool empty { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string swatch { get; set; }
        public string sku { get; set; }
        public string url { get; set; }
   
    }

    public class ProductData
    {
        public ProductData()
        {
            values = new List<string>();
            empty = false;
        }

        public List<string> values;
        public bool empty { get; set; }
    }


}
