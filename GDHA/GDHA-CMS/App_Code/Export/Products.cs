﻿
using System.Linq;
using System.Data;
using umbraco;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Web;


namespace Uniform.GDHA.BusinessLogic.Exports
{
	public class Products
	{
		public static DataTable GetProducts(int nodeId)
		{
			var homepage = uQuery.GetNode(nodeId);
			return GetProducts(homepage);
		}

		public static DataTable GetProducts(Node homepage)
		{
			// construct the table
			var table = new DataTable();
			table.Columns.Add("manufacturer", typeof(string));
			table.Columns.Add("Model", typeof(string));
			table.Columns.Add("sku", typeof(string));
			table.Columns.Add("name", typeof(string)); // max 255 chars
			table.Columns.Add("image_url", typeof(string)); // absolute URL (full size image)
			table.Columns.Add("product_category", typeof(string));
			table.Columns.Add("ean", typeof(string)); // optional
			table.Columns.Add("description", typeof(string)); // optional; max 32,000 chars
			table.Columns.Add("mpn", typeof(string)); // optional; same as sku for GDHA
            table.Columns.Add("colour", typeof(string)); // optional; colour of variant
		    table.Columns.Add("Added",typeof(string));
			table.Columns.Add("Edited",typeof(string));
			
            // get the products
			if (homepage != null)
			{
				var brandName = homepage.Name;

				var domain = library.NiceUrlWithDomain(homepage.Id).TrimEnd(new[] { '/' });
				var products = homepage.GetDescendantNodesByType("ProductModel");

				// loop through products
				foreach (var product in products)
				{
					// loop through variants
					foreach (var variant in product.GetChildNodesByType("ProductSku"))
					{
						var imageUrl = GetProductImage(variant);
						//if (string.IsNullOrWhiteSpace(imageUrl))
						//	continue;

						var row = table.NewRow();

						row["manufacturer"] = brandName;
						row["Model"] = product.Name;
						row["sku"] = variant.Name;
						row["name"] = GetProductName(product);
						row["image_url"] = !string.IsNullOrWhiteSpace(imageUrl) ? string.Concat(domain, imageUrl) : string.Empty;
						row["product_category"] = product.Parent.Name;
						row["ean"] = variant.GetProperty<string>("productEAN");
                        row["description"] = product.GetProperty<string>("bodyText");
						row["mpn"] = variant.Name;
					    row["colour"] = GetColourName(variant.GetProperty<string>("productColour"));
					    row["added"] = product.CreateDate;
						row["Edited"] = product.UpdateDate;
                        table.Rows.Add(row);
					}
				}
			}

			return table;
		}

        private static string GetColourName(string nodeId)
        {
            if (string.IsNullOrWhiteSpace(nodeId)) return "";
            var swatchColour = uQuery.GetNode(nodeId);
            return swatchColour != null ? swatchColour.Name : "";
        }
		private static string GetBrandName(string brand)
		{
			var field = brand.ToString();			

			return brand.ToString();
		}

		private static string GetProductImage(Node node)
		{
			var csv = node.GetProperty<string>("productImages");
			if (!string.IsNullOrWhiteSpace(csv))
			{
				var mediaId = uQuery.GetCsvIds(csv).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(mediaId))
				{
					var mediaNode = uQuery.GetMedia(mediaId);
					if (mediaNode != null)
						return mediaNode.GetImageUrl();
				}
			}

			return string.Empty;
		}

		private static string GetProductName(Node node)
		{
			/*var description = Strings.StripHTML(node.GetProperty<string>("bodyText"));

			if (!string.IsNullOrWhiteSpace(description))
			{
				// if the description starts with the model/name, then just return it; otherwise prefix it.
				return description.StartsWith(node.Name) ? description : string.Concat(node.Name, " - ", description);
			}*/

			return node.Name;
		}
	}
}