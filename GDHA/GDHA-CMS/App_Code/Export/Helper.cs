﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Configuration;
using log4net;


namespace Uniform.GDHA.BusinessLogic.Exports
{
	public class Helper
	{
        private static readonly ILog Log =
        LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType
        );
		public static StringBuilder ToCsv(DataTable table, string delimiter)
		{
			// build the export csv
			var csv = new List<string>();
			var sb = new StringBuilder();

			// make sure there are rows to output before starting
			if (table.Rows.Count > 0)
			{
				// add the column headers
				foreach (DataColumn column in table.Columns)
				{
					csv.Add(string.Format("\"{0}\"", column.ColumnName));
				}
				sb.AppendLine(string.Join(delimiter, csv.ToArray()));

				// add the row data
				foreach (DataRow row in table.Rows)
				{
					csv.Clear();
					foreach (DataColumn column in table.Columns)
					{
						csv.Add(string.Format("\"{0}\"", column.DataType == typeof(string) ? ReplaceCharacters(row[column].ToString()) : row[column]));
					}
					sb.AppendLine(string.Join(delimiter, csv.ToArray()));
				}
			}

			return sb;
		}

		public static void DownloadCsv(string prefix, StringBuilder csv)
		{
			DownloadCsv(prefix, csv.ToString());
		}

		public static void DownloadCsv(string prefix, string csv)
		{
			// ad-hoc file name
			var filename = string.Concat(prefix, DateTime.Now.ToString("yyyyMMddHHmmss"), ".csv");

			// get the bytes from the string
			var bytes = UTF8Encoding.UTF8.GetBytes(csv);

			// send the csv to browser
			var context = HttpContext.Current;
			context.Response.AddHeader("content-disposition", string.Concat("attachment; filename=", filename));
			context.Response.ContentType = "text/csv";
			context.Response.BinaryWrite(bytes);
			context.Response.Flush();
			context.Response.End();
		}

		public static bool UploadCsv(string filename, string csv)
		{
			using (var stream = new MemoryStream(UTF8Encoding.UTF8.GetBytes(csv)))
			{
				return UploadCsv(filename, stream, true);
			}
		}

		public static bool UploadCsv(HttpPostedFile uploadedCsv)
		{
			return UploadCsv(uploadedCsv.FileName, uploadedCsv.InputStream, true);
		}

		private static bool UploadCsv(string filename, Stream stream, bool overwrite)
		{
            var hostname = WebConfigurationManager.AppSettings["reevoo:SftpHostname"];
            Log.Info("Reevoo Hostname:" +hostname);
			var username = WebConfigurationManager.AppSettings["reevoo:SftpUsername"];
            Log.Info("Reevoo Username:" + username);
			var password = WebConfigurationManager.AppSettings["reevoo:SftpPassword"];
            Log.Info("Reevoo Passwor:" + password);
			var path = WebConfigurationManager.AppSettings["reevoo:SftpPath"];
            Log.Info("Reevoo Path:" + path);
		    string[] reevoo = {hostname, username, password, path};
		    foreach (var s in reevoo)
		    {
		        if (string.IsNullOrEmpty(s))
		        {
                    Log.Warn("reevoo settings is empty:" + s);
		            return false;
		        }
		    }

		    try
		    {

		        using (var sftp = new Renci.SshNet.SftpClient(hostname, username, password))
		        {
		            sftp.Connect();

		            if (path.Length > 1 && path.StartsWith("/"))
		                sftp.ChangeDirectory(path);

		            sftp.UploadFile(stream, filename, overwrite);

		            sftp.Disconnect();
		        }

		        return true;
		    }
		    catch
		    {
                Log.Warn("Failed Upload, most likely didnt connect");
                return false;
		    }
		}

		private static string ReplaceCharacters(string input)
		{
			var replaceables = new Dictionary<string, string>()
			{
				{ "£", "GBP" },
				{ "\u2013", "-" },
				{ "\u2014", "-" },
				{ "\u2015", "-" },
				{ "\u2017", "_" },
				{ "\u2018", "'" },
				{ "\u2019", "'" },
				{ "\u2026", "..." },
				{ "\u201a", "," },
				{ "\u201b", "'" },
				{ "\u201c", "\"" },
				{ "\u201d", "\"" },
				{ "\u201e", "\"" },
				{ "\u2032", "'" },
				{ "\u2033", "\"" },
				{ "\"", "\"\"" }
			};

			if (!string.IsNullOrWhiteSpace(input))
			{
				foreach (var replaceable in replaceables)
				{
					if (input.Contains(replaceable.Key))
					{
						input = input.Replace(replaceable.Key, replaceable.Value);
					}
				}
			}

			return input;
		}
	}
}