﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using uLocate.Core.Models;
using uLocate.Core.Extensions;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.IO;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace Uniform.GDHA.BusinessLogic.Exports
{
	public class Retailers
	{
		public static DataTable GetRetailers()
		{
			// construct the table
			var table = new DataTable();
			table.Columns.Add("MEMBER NO", typeof(string));
			table.Columns.Add("NAME", typeof(string));
			table.Columns.Add("ADDRESS", typeof(string));
			table.Columns.Add("TEL NO", typeof(string));
			table.Columns.Add("FAX", typeof(string));
			table.Columns.Add("EMAIL", typeof(string));
			table.Columns.Add("WEBSITE URL", typeof(string));
			table.Columns.Add("Category", typeof(string));
			table.Columns.Add("CiH?", typeof(string));
            table.Columns.Add("Stoves", typeof(string));
            table.Columns.Add("Belling", typeof(string));
            table.Columns.Add("New World", typeof(string));
            table.Columns.Add("LEC", typeof(string));
			table.Columns.Add("Products", typeof(string));
			table.Columns.Add("UmbracoId", typeof(int));
			table.Columns.Add("EktronId", typeof(string));

			// get the retailers
            var retailers = uQuery.GetNodesByXPath("/root/ConfigFolder/IndependentRetailers//Retailer");

			// loop through retailers
			foreach (var retailer in retailers)
			{
				var row = table.NewRow();

				var addressId = retailer.GetProperty<int>("retailerAddress");
				var brands = retailer.GetProperty("brands").Value;

				var categoriesXml = retailer.GetProperty<string>("categories");
				var category = categoriesXml;

				var productsXml = retailer.GetProperty<string>("products");
				var products = productsXml;

                row["MEMBER NO"] = retailer.GetProperty<string>("memberId");
				row["NAME"] = retailer.Name;
                row["ADDRESS"] = retailer.GetProperty<string>("address");
                row["TEL NO"] = retailer.GetProperty<string>("phoneNumber");
                row["FAX"] = retailer.GetProperty<string>("fax");
                row["EMAIL"] = retailer.GetProperty<string>("email");
                row["WEBSITE URL"] = retailer.GetProperty<string>("website");

				row["Category"] = category;
                row["CiH?"] = retailer.GetProperty<string>("cih?");
				row["Stoves"] = brands != null && brands.Contains("Stoves");
                row["Belling"] = brands != null && brands.Contains("Belling");
                row["New World"] = brands != null && brands.Contains("New World");
				row["LEC"] = brands != null && brands.Contains("Lec");

				row["Products"] = products;
				row["UmbracoId"] = retailer.Id;
				row["EktronId"] = retailer.GetProperty<string>("ektronId");

				table.Rows.Add(row);
			}

			return table;
		}

		private static Dictionary<char, int> CreateAlphabetFolders(int rootNodeId)
		{
			var lookup = new Dictionary<char, int>();
			var letters = "1ABCDEFGHIJKLMNOPQRSTUVWXY".ToCharArray();
			var service = UmbracoContext.Current.Application.Services.ContentService;

			foreach (var c in letters)
			{
				var letter = c.ToString();

				var rootNode = service.GetById(rootNodeId);
				var content = rootNode.Children().FirstOrDefault(x => x.Name.StartsWith(letter));

				if (content == null)
				{
					content = service.CreateContent(letter, rootNodeId, "RetailerSubFolder", 1);
					service.Save(content, 1, false);
				}

				lookup.Add(c, content.Id);
			}

			return lookup;
		}

		public static bool ImportRetailerData(HttpPostedFile uploadedFile)
		{
			var path = IOHelper.MapPath("~/App_Data/TEMP/temp_retailer_data.xlsx");

			uploadedFile.SaveAs(path);

			if (!System.IO.File.Exists(path))
				return false;

			var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"", path);
			using (var connection = new OleDbConnection(connectionString))
			using (var adapter = new OleDbDataAdapter("SELECT * FROM [MEMBERS$]", connection))
			using (var data = new DataTable())
			{
				connection.Open();

				adapter.Fill(data);

				var retailerFolderId = 11185; // 1218; 11185
				var lookup = CreateAlphabetFolders(retailerFolderId);

				var service = UmbracoContext.Current.Application.Services.ContentService;

				foreach (DataRow row in data.Rows)
				{
					var memberNumber = row.Field<double?>("MEMBER NO");
					var storeName = row.Field<string>("NAME");
					var storeAddress = row.Field<string>("ADDRESS");
					var storePostcode = row.Field<string>("POST CODE");
					var storeTelephone = row.Field<string>("TEL NO");
					var storeEmail = row.Field<string>("EMAIL");
					var storeCategory = row.Field<string>("Category");
					var isCiH = row.Field<double?>("CiH?");
					var isStoves = row.Field<double?>("Stoves");
					var isBelling = row.Field<double?>("Belling");
					var isNewWorld = row.Field<double?>("New World");
					var isLec = row.Field<double?>("LEC");

					var firstLetter = char.ToUpper(storeName[0]);
					var parentId = retailerFolderId;
					if (lookup.ContainsKey(firstLetter))
						parentId = lookup[firstLetter];

					var content = service.CreateContent(storeName, parentId, "RetailerPage", 1);
					content.SetValue("pageTitle", storeName);
					content.SetValue("retailerMemberNumber", memberNumber.HasValue ? memberNumber.Value.ToString() : string.Empty);
					content.SetValue("bodyText", string.Empty);
					content.SetValue("cih", isCiH == 1);

					// categories
					var categories = new List<int>();
					switch (storeCategory.ToUpper())
					{
						case "BUILT-IN SPECIALIST":
							categories.Add(4141);
							break;

						case "RANGE CENTRE":
						case "RANGE SPECIALIST":
						case "RANGE COOKER SPECIALIST":
							categories.Add(4142);
							break;

						case "KITCHEN RETAILER":
							categories.Add(4143);
							break;

						case "INDEPENDENTS":
						case "INDEPENDENT RETAILER":
							categories.Add(4144);
							break;

						default:
							break;
					}

					if (categories.Count > 0)
						content.SetValue("categories", string.Concat("<XPathCheckBoxList><nodeId>", string.Join("</nodeId><nodeId>", categories), "</nodeId></XPathCheckBoxList>"));

					// brands
					var brands = new List<int>();

					if (isBelling == 1)
						brands.Add(1059);

					if (isLec == 1)
						brands.Add(5793);

					if (isNewWorld == 1)
						brands.Add(5819);

					if (isStoves == 1)
						brands.Add(8908); // 5841; 8908

					if (brands.Count > 0)
						content.SetValue("brands", string.Concat("<XPathCheckBoxList><nodeId>", string.Join("</nodeId><nodeId>", brands), "</nodeId></XPathCheckBoxList>"));

					// geocoded address
					if (!string.IsNullOrWhiteSpace(storeAddress))
					{
						var geocodedAddress = GetGeocodedAddress(storeName, storeAddress, storePostcode, storeTelephone, storeEmail);
						if (geocodedAddress != null)
							content.SetValue("retailerAddress", geocodedAddress.Id.ToString());
					}

					service.Save(content, 1, false);
				}

				connection.Close();
			}

			// tidy up
			if (System.IO.File.Exists(path))
				System.IO.File.Delete(path);

			return true;
		}

		private static GeocodedAddress GetGeocodedAddress(string storeName, string storeAddress, string storePostcode, string storeTelephone, string storeEmail)
		{
			if (string.IsNullOrWhiteSpace(storeAddress))
				return null;

			var parts = storeAddress.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

			string address1 = null;
			string address2 = null;
			string locality = null;
			string region = null;

			switch (parts.Count)
			{
				case 1:
					address1 = parts[0];
					break;

				case 2:
					address1 = parts[0];
					break;

				case 3:
					address1 = parts[0];
					region = parts[1];
					break;

				case 4:
					address1 = parts[0];

					var residential = new[] { "AVENUE", "CLOSE", "LANE", "ROAD", "STREET" };
					if (residential.Any(parts[1].ToUpper().Contains))
					{
						address2 = parts[1];
					}
					else
					{
						locality = parts[1];
					}

					region = parts[2];

					break;

				case 5:
					address1 = parts[0];
					address2 = parts[1];
					locality = parts[2];
					region = parts[3];
					break;

				case 6:
					address1 = parts[0];
					address2 = parts[1];
					locality = string.Concat(parts[2], ", ", parts[3]);
					region = parts[4];
					break;

				case 7:
					address1 = string.Concat(parts[0], ", ", parts[1]);
					address2 = parts[2];
					locality = string.Concat(parts[3], ", ", parts[4]);
					region = parts[5];
					break;

				default:
					break;
			}

			var geocodedAddress = new GeocodedAddress()
			{
				Name = storeName,
				Address1 = address1,
				Address2 = address2,
				Locality = locality,
				Region = region,
				PostalCode = storePostcode,
				CountryCode = "GB"
			};

			geocodedAddress.SaveExtendedData("Email", storeEmail);
			geocodedAddress.SaveExtendedData("Telephone", storeTelephone);

			try { geocodedAddress.LookupGeocode(); }
			catch { /* horrible, yes, but Google Maps API has a 500 request limit - currently no other option */ }

			geocodedAddress.Save();

			return geocodedAddress;
		}
	}
}
