﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Uniform.Web.Models
{
    public sealed class CorpBrochureOrderModel 
    {
        private IPublishedContent content;
        public List<SelectListItem> Referrers;
        public List<SelectListItem> Departments;

        public IPublishedContent Content { get { return content; } }

        public CorpBrochureOrderModel()
            : this(UmbracoContext.Current.PublishedContentRequest.PublishedContent)
        { }

        public CorpBrochureOrderModel(IPublishedContent currentPage)
        {
            this.content = currentPage;

            //this.Referrers = this.BuildDropdown("listOfReferrers");
            //this.Departments = this.BuildDropdown("listOfDepartments");
        }

        public static CorpBrochureOrderModel Create(IPublishedContent currentPage)
        {
            var model = new CorpBrochureOrderModel
            {
                content = currentPage
            };
            //this.Referrers = this.BuildDropdown("listOfReferrers");
            //this.Departments = this.BuildDropdown("listOfDepartments"); 
            return model;
        }

        private List<SelectListItem> BuildDropdown(string propertyName)
        {
            List<SelectListItem> data = new List<SelectListItem>();

            var content1 = this.content.GetPropertyValue<Umbraco.Core.Dynamics.DynamicXml>(propertyName);
            if (content1 != null)
            {
                foreach (var item in content1)
                {
                    data.Add(new SelectListItem
                    {
                        Text = item.InnerText,
                        Value = item.InnerText
                    });
                }
            }

            data.Add(new SelectListItem
            {
                Text = "Other",
                Value = "Other"
            });

            return data;
        }

        public bool ConsentToContact { get; set; }

        public string HeardFromBox { get; set; }

        //[Required]
        [Display(Name = "Where did you hear about us?")]
        public string Heardfrom { get; set; }

        //[Required]
        [Display(Name = "Deliver to GDHA")]
        public bool DeliverToGDHA { get; set; }

       // [Required]
        public string Department { get; set; }

        // [Required]
        [Display(Name = "Company")]
        public string Company { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string Surname { get; set; }

        // [Required]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }

        // [Required]
        [Display(Name = "Phone number")]
        public string Telephone { get; set; }

        [Required]
        [Display(Name = "Property Name or Number.")]
        public string PropertyNumber { get; set; }

        [Required]
        public string Street { get; set; }

        [Required]
        public string Town { get; set; }
        
        public string County { get; set; }

        [Required]
        public string Postcode { get; set; }

        
    }
}
