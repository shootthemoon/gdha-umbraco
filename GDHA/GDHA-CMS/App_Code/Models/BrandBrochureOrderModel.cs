﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Archetype.Models;
using Umbraco.Web;

namespace Uniform.Web.Models
{
    public sealed class BrandBrochureOrderModel
    {
        //private IQueryable<Umbraco.Core.Models.IPublishedContent> queryable;

        public BrandBrochureOrderModel()
        {
            this.Brochurers = new List<SelectListItem>();
        }

        public static BrandBrochureOrderModel Create(Umbraco.Core.Models.IPublishedContent currentPage)
        {
            var debug = currentPage.AncestorOrSelf(1);            
            var item = debug.Children.FirstOrDefault(x => x.Name == "Brochure").GetPropertyValue<ArchetypeModel>("brochureListing");
            var count = 0;            

            return new BrandBrochureOrderModel {
                AvailableBrochurers = item.Select(fieldset => new SelectListItem {Value = (count++).ToString(), Text = fieldset.HasValue("title") ? fieldset.GetValue("title") : ""}).ToList(),
            };
        }

		[Required]
		public string Title { get; set; }

		[Required]
		public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "Please enter a valid Phone number")]
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string Telephone { get; set; }
        
        [Required]
        [Display(Name = "Property Name or Number.")]
        public string PropertyNumber { get; set; }

        [Required]
        public string Street { get; set; }
        
        [Required]
        public string Town { get; set; }
        
        [Required]
        public string County { get; set; }
                
        [Required]
        public string Postcode { get; set; }
        
        [Display(Name = "Do you give your consent for Glen Dimplex Home Appliances to contact you in the future with details of products and services, which we think will be of interest to you? ")]
        public bool GivenConsentToBeContacted { get; set; }

        public ICollection<SelectListItem> Brochurers { get; set; }
        
        public IList<SelectListItem> AvailableBrochurers { get; set; }
       // [Required(ErrorMessage = "Please select a brochure")]
        public PostedBrochurers PostedBrochurers { get; set; }
    }

    public class PostedBrochurers
    {
        public string[] BrochureIDs { get; set; }
    }
}
