﻿using System.ComponentModel.DataAnnotations;

namespace Uniform.Web.Models
{
	public class NewsletterFormModel
	{
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }
	}
}