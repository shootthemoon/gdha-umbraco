﻿using System.Runtime.Serialization;
using uLocate.Core.Models;

namespace Uniform.Web.Models
{
	public class RetailerAddress : GeocodedAddress
	{
		public RetailerAddress(GeocodedAddress address)
		{
			this.Address1 = address.Address1;
			this.Address2 = address.Address2;
			this.Comment = address.Comment;
			this.Coordinate = address.Coordinate;
			this.CountryCode = address.CountryCode;
			this.CreateDate = address.CreateDate;
			this.Email = address.Email;
			this.Fax = address.Fax;
			this.GeocodeStatus = address.GeocodeStatus;
			this.Id = address.Id;
			this.Locality = address.Locality;
			this.Name = address.Name;
			this.PostalCode = address.PostalCode;
			this.Region = address.Region;
			this.Telephone = address.Telephone;
			this.UpdateDate = address.UpdateDate;
			this.Viewport = address.Viewport;
			this.WebsiteUrl = address.WebsiteUrl;
		}

        public RetailerAddress()
        {

        }

		[IgnoreDataMember]
		public double MilesAway { get; set; }

        [DataMember]
		public bool IsElite { get; set; }

		[DataMember]
		public string DistanceAway
		{
			get { return ExtendedDataValue("DistanceAway"); }
			set { SaveExtendedData("DistanceAway", value); }
		}

		[DataMember]
		public string NiceUrl
		{
			get { return ExtendedDataValue("NiceUrl"); }
			set { SaveExtendedData("NiceUrl", value); }
		}

		[DataMember]
		public string RetailerCategories
		{
			get { return ExtendedDataValue("RetailerCategories"); }
			set { SaveExtendedData("RetailerCategories", value); }
		}

        public string Lng { get; set; }

        public string Lat { get; set; }
    }
}