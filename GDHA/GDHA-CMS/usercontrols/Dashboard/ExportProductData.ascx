﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="umbraco" %>
<%@ Import Namespace="Uniform.GDHA.BusinessLogic.Exports" %>
<%@ Register Assembly="controls" Namespace="umbraco.uicontrols" TagPrefix="umbraco" %>

<script runat="server">
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!Page.IsPostBack)
		{
			var items = new List<ListItem>() { new ListItem(string.Concat(umbraco.ui.Text("choose"), "..."), string.Empty) };
            items.AddRange(umbraco.uQuery.GetNodesByType("USNHomepage").Select(x => new ListItem(x.Name, x.Id.ToString())));

			this.ddlBrands.Items.Clear();
			this.ddlBrands.Items.AddRange(items.ToArray());
		}
	}

	protected void btnExportProducts_Click(object sender, EventArgs e)
	{
		if (this.phFeedback.Visible)
			this.phFeedback.Visible = false;

		var nodeId = -1;
		if (!int.TryParse(this.ddlBrands.SelectedValue, out nodeId))
		{
			this.fbMessage.type = Feedback.feedbacktype.error;
			this.fbMessage.Text = "Please select a brand and try again.";

			this.phForm.Visible = true;
			this.phFeedback.Visible = true;
			return;
		}

		var homepage = uQuery.GetNode(nodeId);
		if (homepage == null)
			return;

		var table = Products.GetProducts(homepage);
		var sb = Helper.ToCsv(table, ",");

		if (sb.Length > 0)
		{
			var brandName = homepage.Name;
			if (this.rblOptions.SelectedValue == "upload")
			{
				try
				{
					if (Helper.UploadCsv(string.Concat(brandName, "-product-export", ".csv"), sb.ToString()))
					{
						this.fbMessage.type = Feedback.feedbacktype.success;
						this.fbMessage.Text = "The products export was successfully uploaded";
					}
					else
					{
						this.fbMessage.type = Feedback.feedbacktype.notice;
						this.fbMessage.Text = "There was a problem uploading the products export. Please try again, otherwise contact technical support.";
					}
				}
				catch (Exception ex)
				{
					this.fbMessage.type = Feedback.feedbacktype.error;
					this.fbMessage.Text = string.Concat("Error: ", ex);
				}

				this.phForm.Visible = true;
				this.phFeedback.Visible = true;
			}
			else
			{
				Helper.DownloadCsv(string.Concat(brandName, "-product-export_"), sb);
			}
		}
		else
		{
			this.fbMessage.type = Feedback.feedbacktype.error;
			this.fbMessage.Text = "There is no data to export. Please select a brand and try again.";

			this.phForm.Visible = true;
			this.phFeedback.Visible = true;
		}
	}

	protected void btnUploadCsv_Click(object sender, EventArgs e)
	{
		if (!this.fuCsv.HasFile)
		{
			this.fbMessage.type = Feedback.feedbacktype.error;
			this.fbMessage.Text = "There is no data to upload. Please select a file and try again.";
		}
		else
		{
			if (Helper.UploadCsv(this.fuCsv.PostedFile))
			{
				this.fbMessage.type = Feedback.feedbacktype.success;
				this.fbMessage.Text = "The products export was successfully uploaded";
			}
			else
			{
				this.fbMessage.type = Feedback.feedbacktype.notice;
				this.fbMessage.Text = "There was a problem uploading the products export. Please try again, otherwise contact technical support.";
			}
		}

		this.phForm.Visible = true;
		this.phFeedback.Visible = true;
	}
</script>

<div class="umb-pane ng-scope">
	<h2>Reevoo Product Data Export</h2>
	<img src="/usercontrols/Dashboard/reevoo-logo.png" alt="Reevoo Logo" class="dashboardIcon" />
    <br/>
	<asp:PlaceHolder runat="server" ID="phForm">
		<p>To export products for the Reevoo service, select a brand, then press the 'Export Products' button.</p>
		<div class="field">
			<asp:Label runat="server" AssociatedControlID="ddlBrands">Select brand: </asp:Label>
			<asp:DropDownList runat="server" ID="ddlBrands" />
			<asp:RadioButtonList runat="server" ID="rblOptions" CssClass="umb-radiobuttons" RepeatLayout="Flow" RepeatColumns="2" RepeatDirection="Horizontal">
				<asp:ListItem Text="Download CSV" Value="download" Selected="true" />
				<asp:ListItem Text="Automatic Upload to Reevoo" Value="upload" />
			</asp:RadioButtonList>
			<asp:Button runat="server" ID="btnExportProducts" Text="Export Products" OnClick="btnExportProducts_Click" />
		</div>
		<br />
		<div class="umb-property ng-scope">
			<div class="control-group umb-control-group">
				<div class="">
					<h3>Manually Upload</h3>
					<p>You can manually upload a CSV export to the Reevoo secure FTP area:</p>
					<div class="field">
						<asp:FileUpload runat="server" ID="fuCsv" />
						<asp:Button runat="server" ID="btnUploadCsv" Text="Upload to Reevoo" OnClick="btnUploadCsv_Click" />
					</div>
				</div>
			</div>
		</div>
	</asp:PlaceHolder>

	<asp:PlaceHolder runat="server" ID="phFeedback" Visible="false">
		<div class="field">
			<umbraco:Feedback runat="server" ID="fbMessage" />
		</div>
	</asp:PlaceHolder>
</div>
