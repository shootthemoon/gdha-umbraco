﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="umbraco" %>
<%@ Import Namespace="Uniform.GDHA.BusinessLogic.Exports" %>
<%@ Register Assembly="controls" Namespace="umbraco.uicontrols" TagPrefix="umbraco" %>

<script runat="server">
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!Page.IsPostBack)
		{
		}
	}

	protected void btnExportRetailers_Click(object sender, EventArgs e)
	{
		if (this.phFeedback.Visible)
			this.phFeedback.Visible = false;

		var table = Retailers.GetRetailers();
		var sb = Helper.ToCsv(table, ",");

		if (sb.Length > 0)
		{
			Helper.DownloadCsv("retailer-export_", sb);
		}
		else
		{
			this.fbMessage.type = Feedback.feedbacktype.error;
			this.fbMessage.Text = "There is no retailer data to export.";

			this.phExportRetailersForm.Visible = true;
			this.phFeedback.Visible = true;
		}
	}

	protected void btnImportRetailers_Click(object sender, EventArgs e)
	{
		if (!this.fuExcel.HasFile)
		{
			this.fbMessage.type = Feedback.feedbacktype.error;
			this.fbMessage.Text = "There is no retailer data to upload. Please select a file and try again.";
		}
		if (this.fuExcel.HasFile && !this.fuExcel.FileName.EndsWith(".xlsx"))
		{
			this.fbMessage.type = Feedback.feedbacktype.error;
			this.fbMessage.Text = "The retailer data does not appear to be an Excel spreadsheet. Please select the appropriate file and try again.";
		}
		else
		{
			if (Retailers.ImportRetailerData(this.fuExcel.PostedFile))
			{
				this.fbMessage.type = Feedback.feedbacktype.success;
				this.fbMessage.Text = "The retailer data was successfully imported.";
			}
			else
			{
				this.fbMessage.type = Feedback.feedbacktype.notice;
				this.fbMessage.Text = "There was a problem importing the retailer data. Please try again, otherwise contact technical support.";
			}
		}

		this.phImportRetailersForm.Visible = true;
		this.phFeedback.Visible = true;
	}
</script>

<div class="dashboardWrapper">
    <img src="/umbraco/assets/img/application/logo.png" alt="Logo" />
	<h2>Retailers</h2>
	<asp:PlaceHolder runat="server" ID="phFeedback" Visible="false">
		<div class="field">
			<umbraco:Feedback runat="server" ID="fbMessage" />
		</div>
	</asp:PlaceHolder>
	<div class="dashboardColWrapper">
		<div class="dashboardCols">
			<div class="">
				<h3>Export Retailers</h3>
				<asp:PlaceHolder runat="server" ID="phExportRetailersForm">
					<p>To export retailers, press the 'Export Retailers' button:</p>
					<div class="field">
						<asp:Label runat="server" AssociatedControlID="btnExportRetailers"></asp:Label>
						<asp:Button runat="server" ID="btnExportRetailers" Text="Export Retailers" OnClick="btnExportRetailers_Click" />
					</div>
				</asp:PlaceHolder>
			</div>
			<!--<div class="dashboardCol third">
				<h3>Import Retailers</h3>
				<asp:PlaceHolder runat="server" ID="phImportRetailersForm">
					<p>To import retailers, select the Excel spreadsheet, then press the 'Import Retailers' button:</p>
					<div class="field">
						<asp:FileUpload runat="server" ID="fuExcel" />
						<asp:Button runat="server" ID="btnImportRetailers" Text="Import Retailers" OnClick="btnImportRetailers_Click" />
					</div>
				</asp:PlaceHolder>
			</div>-->
			<!--<div class="dashboardCol third last"></div-->
		</div>
	</div>
</div>
